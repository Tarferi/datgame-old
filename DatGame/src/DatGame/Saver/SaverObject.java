package DatGame.Saver;

import java.util.LinkedHashMap;
import java.util.Map;

import DatGame.Saver.JSON.Json;
import DatGame.Saver.JSON.JsonObject;
import DatGame.Saver.JSON.JsonObject.Member;
import DatGame.Saver.JSON.JsonValue;

public class SaverObject extends Saver {

	private JsonObject obj;

	protected SaverObject(JsonObject obj) {
		this.obj = obj;
	}

	public SaverObject() {
		this.obj = new JsonObject();
	}

	public void addObject(String key, String s) {
		obj.add(key, s);
	}

	public void addObject(String key, boolean s) {
		obj.add(key, s);
	}

	public void addObject(String key, byte s) {
		obj.add(key, super.numberAsValue(s));
	}

	public void addObject(String key, char s) {
		obj.add(key, s);
	}

	public void addObject(String key, short s) {
		obj.add(key, super.numberAsValue(s));
	}

	public void addObject(String key, int s) {
		obj.add(key, super.numberAsValue(s));
	}

	public void addObject(String key, long s) {
		obj.add(key, super.numberAsValue(s));
	}

	public void addObject(String key, float s) {
		obj.add(key, super.numberAsValue(s));
	}

	public void addObject(String key, double s) {
		obj.add(key, super.numberAsValue(s));
	}

	public void addObject(String key, Saver object) {
		obj.add(key, object.getJsonObject());
	}

	public void addObject(String key, Saveable s) {
		JsonValue v = Json.parse(s.getSaveString());
		obj.add(key, v);
	}

	@Override
	protected JsonValue getJsonObject() {
		return obj;
	}

	@Override
	public Map<String, Object> getObject(boolean eval) {
		Map<String, Object> a = new LinkedHashMap<>();
		for (Member q : obj) {
			if (eval) {
				a.put(q.getName(), super.getRegularObject(q.getValue(), eval));
			} else {
				if (q.getValue().isArray()) {
					a.put(q.getName(), new SaverArray(q.getValue().asArray()));
				} else if (q.getValue().isObject()) {
					if (super.isNumericValue(q.getValue().asObject())) {
						a.put(q.getName(), super.getRegularObject(q.getValue(), eval));
					} else {
						a.put(q.getName(), new SaverObject(q.getValue().asObject()));
					}
				} else {
					a.put(q.getName(), super.getRegularObject(q.getValue(), eval));
				}
			}
		}
		return a;
	}
}
