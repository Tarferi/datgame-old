package DatGame.Saver;

import java.io.File;
import java.io.FileOutputStream;

public abstract class Saveable {

	protected Saveable() {
	}

	protected abstract String getSaveString();

	protected abstract Saveable restoreFromObject(Saver s);

	/**
	 * Gets saver of current saveable object.
	 * Use this to wrap data around currently being saved saveable only
	 * @return
	 */
	public abstract Saver getSaver();

	/**
	 * Returns True if object can be saved to file
	 * Default is True
	 * @return True if object can be saved to file
	 */
	protected boolean canBeSaved() { //TODO: Remove final
		return true;
	}

	public final void save(File f) {
		if (canBeSaved()) {
			FileOutputStream o = null;
			try {
				if (f.exists()) {
					f.delete();
				}
				f.createNewFile();
				o = new FileOutputStream(f);
				o.write(getSaveString().getBytes()); //TODO: No saving
				o.close();
				o = null;
				return;
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (o != null) {
				try {
					o.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
