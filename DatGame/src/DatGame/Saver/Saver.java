package DatGame.Saver;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import DatGame.Saver.JSON.Json;
import DatGame.Saver.JSON.JsonObject;
import DatGame.Saver.JSON.JsonValue;

public abstract class Saver {
	protected abstract JsonValue getJsonObject();

	public static final SaverArray arrayFromFile(File f) {
		if (f.exists()) {
			try {
				String s = new String(Files.readAllBytes(f.toPath()));
				JsonValue jsn = Json.parse(s);
				if (!jsn.isArray()) {
					System.err.println("Failed to parse root array: object is not array: " + jsn);
					return null;
				} else {
					return new SaverArray(jsn.asArray());
				}
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Loading from non existing file: " + f);
			return null;
		}
	}

	public static final SaverObject objectFromFile(File f) {
		if (f.exists()) {
			try {
				String s = new String(Files.readAllBytes(f.toPath()));
				JsonValue jsn = Json.parse(s);
				if (!jsn.isObject()) {
					System.err.println("Failed to parse root array: object is not array: " + jsn);
					return null;
				} else {
					return new SaverObject(jsn.asObject());
				}
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		} else {
			System.err.println("Loading from non existing file: " + f);
			return null;
		}
	}

	/**
	 * Evaluate nested objects
	 * @param eval True to evaluate nested objects
	 * @return Object representation of saved data
	 */
	public abstract Object getObject(boolean eval);

	protected boolean isNumericValue(JsonObject o) {
		return o.get("#TYPE#") != null && o.get("#VALUE#") != null;
	}

	protected Object getRegularObject(JsonValue v, boolean eval) {
		if (v.isArray()) {
			return new SaverArray(v.asArray()).getObject(eval);
		} else if (v.isObject()) {
			JsonObject o = v.asObject();
			JsonValue p, q;
			if ((p = o.get("#TYPE#")) != null) {
				if ((q = o.get("#VALUE#")) != null) {
					String k = p.asString();
					if (k.equals("byte")) {
						return (byte) q.asInt();
					} else if (k.equals("short")) {
						return (short) q.asInt();
					} else if (k.equals("integer")) {
						return (int) q.asInt();
					} else if (k.equals("long")) {
						return (long) q.asInt();
					} else if (k.equals("float")) {
						return Float.parseFloat(q.asString());
					} else if (k.equals("double")) {
						return Double.parseDouble(q.asString());
					} else if (k.equals("char")) {
						return (char) q.asInt();
					} else {
						System.err.println("Undeterminable numeric type: " + p);
					}
				}
			}
			return new SaverObject(v.asObject()).getObject(eval);
		} else if (v.isBoolean()) {
			return v.asBoolean();
		} else if (v.isFalse()) {
			return false;
		} else if (v.isNull()) {
			return null;
		} else if (v.isNumber()) { // Should only happen to integer
			return v.asInt();
		} else if (v.isString()) {
			return v.asString();
		} else if (v.isTrue()) {
			return true;
		} else {
			System.err.println("Undeterminable type: " + v);
		}
		return null;
	}

	protected JsonValue numberAsValue(Object number) {
		JsonObject o = new JsonObject();
		if (number instanceof Byte) {
			o.add("#TYPE#", "byte");
			o.add("#VALUE#", (byte) number);
		} else if (number instanceof Short) {
			o.add("#TYPE#", "short");
			o.add("#VALUE#", (short) number);
		} else if (number instanceof Integer) {
			return Json.value((int) number);
			// o.add("#TYPE#", "integer");
			// o.add("#VALUE#", (int) number);
		} else if (number instanceof Long) {
			o.add("#TYPE#", "long");
			o.add("#VALUE#", (long) number);
		} else if (number instanceof Float) {
			o.add("#TYPE#", "float");
			o.add("#VALUE#", ((float) number) + "");
		} else if (number instanceof Double) {
			o.add("#TYPE#", "double");
			o.add("#VALUE#", ((double) number) + "");
		} else if (number instanceof Character) {
			o.add("#TYPE#", "char");
			o.add("#VALUE#", (int) number);
		} else {
			System.err.println("Undetermined object numberic type: " + number);
			return null;
		}
		return o;
	}
}
