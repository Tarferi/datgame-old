package DatGame.Saver;

import DatGame.Saver.JSON.Json;
import DatGame.Saver.JSON.JsonArray;

public abstract class SaveableArray extends Saveable {

	private SaverArray sobj;

	protected abstract void commitSave(SaverArray s);

	@Override
	protected final String getSaveString() {
		JsonArray obj = Json.array().asArray();
		sobj = new SaverArray(obj);
		commitSave(sobj);
		return obj.toString();
	}

	public abstract Saveable restoreFromArray(SaverArray arr);

	@Override
	protected final Saveable restoreFromObject(Saver s) {
		if (!(s instanceof SaverArray)) {
			System.err.println("Invalid root save type: " + s);
			return null;
		} else {
			return restoreFromArray((SaverArray) s);
		}
	}

	public final Saver getSaver() {
		if (sobj == null) {
			JsonArray obj = Json.array().asArray();
			sobj = new SaverArray(obj);
			commitSave(sobj);
		}
		return sobj;
	}
}
