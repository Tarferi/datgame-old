package DatGame.Saver;

import java.util.List;

import DatGame.Saver.JSON.JsonArray;
import DatGame.Saver.JSON.JsonValue;

public class SaverArray extends Saver {

	private final JsonArray obj;

	public SaverArray() {
		obj = new JsonArray();
	}

	public void addObject(String s) {
		obj.add(s);
	}

	public void addObject(boolean s) {
		obj.add(s);
	}

	public void addObject(byte s) {
		obj.add(super.numberAsValue(s));
	}

	public void addObject(char s) {
		obj.add(s);
	}

	public void addObject(short s) {
		obj.add(super.numberAsValue(s));
	}

	public void addObject(int s) {
		obj.add(super.numberAsValue(s));
	}

	public void addObject(long s) {
		obj.add(super.numberAsValue(s));
	}

	public void addObject(float s) {
		obj.add(super.numberAsValue(s));
	}

	public void addObject(double s) {
		obj.add(super.numberAsValue(s));
	}

	public void addObject(String[] s) {
		for (String q : s) {
			obj.add(q);
		}
	}

	public void addObject(boolean[] s) {
		for (boolean q : s) {
			obj.add(q);
		}
	}

	public void addObject(byte[] s) {
		for (byte q : s) {
			obj.add(super.numberAsValue(q));
		}
	}

	public void addObject(char[] s) {
		for (char q : s) {
			obj.add(super.numberAsValue(q));
		}
	}

	public void addObject(short[] s) {
		for (short q : s) {
			obj.add(super.numberAsValue(q));
		}
	}

	public void addObject(int[] s) {
		for (int q : s) {
			obj.add(super.numberAsValue(q));
		}
	}

	public void addObject(long[] s) {
		for (long q : s) {
			obj.add(super.numberAsValue(q));
		}
	}

	public void addObject(float[] s) {
		for (float q : s) {
			obj.add(super.numberAsValue(q));
		}
	}

	public void addObject(double[] s) {
		for (double q : s) {
			obj.add(super.numberAsValue(q));
		}
	}

	public void addObject(Saver s) {
		obj.add(s.getJsonObject());
	}

	public void addObject(Saver[] s) {
		for (Saver q : s) {
			obj.add(q.getJsonObject());
		}
	}

	protected SaverArray(JsonArray a) {
		obj = a;
	}

	@Override
	public Object[] getObject(boolean eval) {
		List<JsonValue> ar = obj.values();
		Object[] data = new Object[ar.size()];
		int i = 0;
		for (JsonValue q : ar) {
			if (eval) {
				data[i] = super.getRegularObject(q, eval);
			} else {
				if (q.isArray()) {
					data[i] = new SaverArray(q.asArray());
				} else if (q.isObject()) {
					if (super.isNumericValue(q.asObject())) {
						data[i] = super.getRegularObject(q.asObject(), eval);
					} else {
						data[i] = new SaverObject(q.asObject().asObject());
					}
				} else {
					data[i] = super.getRegularObject(q, eval);
				}
			}
			i++;
		}
		return data;
	}

	@Override
	protected JsonValue getJsonObject() {
		return obj;
	}

	public void addObject(String string, SaverArray a) {
		// TODO Auto-generated method stub

	}
}
