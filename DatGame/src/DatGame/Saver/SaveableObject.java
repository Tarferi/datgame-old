package DatGame.Saver;

import DatGame.Saver.JSON.Json;
import DatGame.Saver.JSON.JsonObject;

public abstract class SaveableObject extends Saveable {

	private SaverObject sobj;

	protected abstract void commitSave(SaverObject s);

	@Override
	protected final String getSaveString() {
		JsonObject obj = Json.object();
		sobj = new SaverObject(obj);
		commitSave(sobj);
		return obj.toString();
	}

	public abstract Saveable restoreFromObject(SaverObject obj);

	@Override
	protected final Saveable restoreFromObject(Saver s) {
		if (!(s instanceof SaverObject)) {
			System.err.println("Invalid root save type: " + s);
			return null;
		} else {
			return restoreFromObject((SaverObject) s);
		}
	}

	public final Saver getSaver() {
		if(sobj==null) {
			JsonObject obj = Json.object();
			sobj=new SaverObject(obj);
			commitSave(sobj);
		}
		return sobj;
	}

}
