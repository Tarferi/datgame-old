package DatGame.Area;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import DatGame.Area.Chunks.AreaChunk;
import DatGame.Attributes.Location;
import DatGame.Attributes.Size;
import DatGame.Attributes.Vector;
import DatGame.Objects.GameObject;
import DatGame.Objects.Player;
import DatGame.Objects.Projectile;
import DatGame.Objects.WeaponItem;
import DatGame.Saver.Saveable;
import DatGame.Saver.SaverObject;

public class EmptyArea extends Area {

	private static final int fontHeight = 12;
	private static int fontWidth = -1;
	private static final Font font = new Font("Monospaced", Font.PLAIN, fontHeight);

	private int color;
	private Color fc; // Background color
	private Color nc; // Inverse color

	public EmptyArea() {
		super(EmptyArea.class, Vector.getZeroVector(), null, null);
	}

	public EmptyArea(Object data, Vector location, Map map, AreaChunk chunk) {
		super(EmptyArea.class, location, map, chunk);
		try {
			color = Integer.parseInt(data.toString());
		} catch (Exception e) {
			color = (int) (Math.random() * Integer.MAX_VALUE);
		}
		this.fc = new Color(color);
		this.nc = new Color(color ^ 0xffffff);
	}

	@Override
	protected int getAreaTypeID() {
		return 1;
	}

	@Override
	public String getAreaName() {
		return "Void";
	}

	private void drawSymbol(Graphics g, Color c, char h, Location l) {
		drawSymbol(g, c, h, l.getValue(0), l.getValue(1));
	}

	private void drawSymbol(Graphics g, Color c, char h, int x, int y) {
		g.setColor(nc); // Not that input color does not matter
		g.drawString(h + "", x, y);
	}

	@Override
	protected void drawObject(Graphics g, GameObject o) {
		drawSymbol(g, Color.orange, '?', o.getLocation());
	}

	@Override
	protected void drawPlayer(Graphics g, Player p) {
		g.setColor(nc);
		drawSymbol(g, Color.green, 'P', p.getLocation());
	}

	@Override
	protected void drawWeapon(Graphics g, WeaponItem p) {
		drawSymbol(g, Color.orange, '?', p.getLocation());
	}

	@Override
	protected void drawProjectile(Graphics g, Projectile p) {
		drawSymbol(g, Color.orange, '?', p.getLocation());
	}

	@Override
	protected void repaint(Graphics g, Vector size) {
		g.setFont(font);
		g.setColor(fc);
		if (fontWidth == -1) { // First paint of all, need to determine width of each letter
			fontWidth = g.getFontMetrics().stringWidth("@");
		}
		g.fillRect(0, 0, size.getValue(0), size.getValue(1));
		g.setColor(nc); // Next to be drawn are the entities in inverse color
	}

	@Override
	protected Area generateNewArea(Vector location, Map map, AreaChunk area) {
		return new EmptyArea("invalid", location, map, area);
	}

	@Override
	protected void commitSave(SaverObject s) {
		SaverObject a = new SaverObject();
		super.commitSave(a);
		s.addObject("AreaData", a);
		s.addObject("Color", color);
	}

	@Override
	public Saveable restoreFromObject(SaverObject obj) {
		java.util.Map<String, Object> aq = obj.getObject(false);
		SaverObject a = (SaverObject) aq.get("AreaData");
		EmptyArea q = (EmptyArea) super.restoreFromObject(a);
		if (q != null) {
			q.color = (int) aq.get("Color");
		}
		return q;
	}

	@Override
	protected Size getObjectSize(GameObject o) {
		return new Size(o, new Vector().setValue(0, fontWidth).setValue(1, fontHeight));
	}

}
