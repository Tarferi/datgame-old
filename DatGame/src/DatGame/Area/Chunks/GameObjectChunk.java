package DatGame.Area.Chunks;

import DatGame.Area.Area;
import DatGame.Area.Map;
import DatGame.Attributes.Vector;
import DatGame.Objects.GameObject;

public class GameObjectChunk {
	/**
	 * loadedChunks array length
	 * This is respectful to dimensions
	 */
	private static int loadDimensionTotal = -1;
	/**
	 * Contains index in the middle of loadedChunks array
	 * This is respectful to dimensions
	 */
	private static final int loadDimension = 3;

	private GameObject object;
	private Map map;
	private final Vector[] vectors;
	private final AreaChunk[] loadedChunks;

	public GameObjectChunk(GameObject object, Map map) {
		if (loadDimensionTotal == -1) {
			loadDimensionTotal = 1;
			for (int i = 0, o = Vector.getDimensions(); i < o; i++) {
				loadDimensionTotal *= loadDimension;
			}
		}
		loadedChunks = new AreaChunk[loadDimensionTotal];
		vectors = new Vector[loadDimensionTotal];
		this.object = object;
		this.map = map;
		object.getLocation().getArea().getChunk().getChunk().commitAppear(object);
	}

	public Map getMap() {
		return map;
	}

	public GameObject getObject() {
		return object;
	}

	private DataChunk lastKnownChunk;

	private Vector lastKnownLocation;

	public void objectMoved() {
		Vector location = object.getLocation().getArea().getAreaLocation(); // Location of area within other areas
		if (location != lastKnownLocation) {
			lastKnownLocation = location;
			Vector leftMost = location.getClone().divideVectorNeg(Vector.getUnitVector(loadDimension)); // Location of AreaChunk of the object
			leftMost.subtractVector(Vector.getUnitVector(loadDimension / 2));
			craftDimensions(loadedChunks, leftMost);
			DataChunk currentChunk = object.getLocation().getArea().getChunk().getChunk();
			if (lastKnownChunk != currentChunk) {
				if (lastKnownChunk != null) {
					lastKnownChunk.commitDisposal(object);
				}
				lastKnownChunk = currentChunk;
				lastKnownChunk.commitAppear(object);
			}
		}
	}

	private void craftDimensions(AreaChunk[] object, Vector leftMost) {
		for (int i = 0; i < object.length; i++) {
			Vector nw = leftMost.getClone().addSubstantionalValue(leftMost, loadDimension, i);
			vectors[i] = nw.getClone();
			nw.multiplyVector(Vector.getUnitVector(AreaChunk.ChunkSize));
			object[i] = AreaChunk.getAreaChunkForArea(map, nw);
		}
	}

	public void deallocate() {
		for (AreaChunk c : loadedChunks) {
			c.deallocate();
		}

	}

	public Area getArea(Vector location) {
		Vector vec = location.getClone().divideVectorNeg(Vector.getUnitVector(loadDimension));
		for (int i = 0; i < vectors.length; i++) {
			if (vectors[i].equals(vec)) {
				return loadedChunks[i].getArea(location);
			}
		}
		System.err.println("Requesting area outside the game object area. Perhaps movement was forgotten?");
		// Nothing was found, try to move object and if it fails again, it fails in general
		objectMoved();
		for (int i = 0; i < vectors.length; i++) {
			if (vectors[i].equals(vec)) {
				return loadedChunks[i].getArea(location);
			}
		}
		System.err.println("Requesting area outside the game object area");
		return null;
	}

}
