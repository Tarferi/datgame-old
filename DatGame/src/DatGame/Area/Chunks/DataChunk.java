package DatGame.Area.Chunks;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import DatGame.Area.Map;
import DatGame.Attributes.Vector;
import DatGame.Objects.GameObject;
import DatGame.Saver.Saveable;
import DatGame.Saver.SaveableObject;
import DatGame.Saver.Saver;
import DatGame.Saver.SaverArray;
import DatGame.Saver.SaverObject;

/**
 * Data chunk contains multiple AreaChunks allowing the save and loade to be done at once
 * @author Drew
 *
 */
public class DataChunk extends SaveableObject {
	/**
	 * Number of AreaChunks to be stored  in a dimension
	 * Meaning that if set to 3, the DataChunk will contain 3^Dimension AreaChunks
	 */
	public static final int DataChunkSize = 3;
	private static int ChunkSizeTotal = -1;
	private static final Vector DataChunkSizeVector = Vector.getUnitVector(DataChunkSize);
	private final Vector firstAreaChunk;

	private final AreaChunk[] areachunks;
	private Map map;
	private File chunkFile;

	private final boolean valid;
	private int references = 0;
	private final Vector[] vectors;

	private DataChunk(Map map) {
		this.map = map;
		this.areachunks = null;
		this.chunkFile = null;
		this.firstAreaChunk = null;
		this.valid = false;
		this.vectors = null;
	}

	static {
		ChunkSizeTotal = 1;
		for (int i = 0, o = Vector.getDimensions(); i < o; i++) {
			ChunkSizeTotal *= DataChunkSize;
		}
	}

	private DataChunk(Map map, Vector whichone, File chunkFile) {
		this(map, whichone, chunkFile, null);
	}

	/**
	 * Calculates the locations of contained areas
	 * @param left Leftmost (Location with all dimensions set to least possible values)
	 * @return
	 */
	private static Vector[] getAreaChunkVectorArray(Vector left) {
		left = left.getClone().multiplyVector(DataChunkSizeVector);
		Vector[] areas = new Vector[ChunkSizeTotal];
		int theSize = DataChunkSize;
		for (int i = 0; i < areas.length; i++) { // For all contained vectors
			Vector v = left.getClone(); // Clone current vector
			v.addSubstantionalValue(left, theSize, i); // Shift the current location by i
			areas[i] = v;
		}
		return areas;
	}

	private AreaChunk[] getNewChunkArea(Vector first) {
		AreaChunk[] area = new AreaChunk[ChunkSizeTotal];
		for (int i = 0; i < area.length; i++) {
			area[i] = new AreaChunk(this, vectors[i].getClone().multiplyVector(DataChunkSizeVector));
		}
		return area;
	}

	private DataChunk(Map map, Vector whichone, File chunkFile, AreaChunk[] chunks) {
		this.map = map;
		this.chunkFile = chunkFile;
		this.firstAreaChunk = whichone;
		this.valid = true;
		this.vectors = getAreaChunkVectorArray(whichone);
		if (chunks == null) {
			areachunks = getNewChunkArea(whichone);
			save(chunkFile);
		} else {
			this.areachunks = chunks;
		}
	}

	private static final java.util.Map<Object, DataChunk> loadedChunks = new HashMap<>();

	/**
	 * Returns index to data area of vector
	 * @param v
	 * @return
	 */
	private final int getAreaChunkIndex(Vector v) {
		for (int i = 0; i < vectors.length; i++) {
			if (vectors[i].equals(v)) {
				return i;
			}
		}
		System.err.println("Invalid vector search in DataChunk");
		return 0;
	}

	public boolean isValid() {
		return valid;
	}

	public Map getMap() {
		return map;
	}

	/**
	 * Returns chunk file name in map chunks directory
	 * @param location Absolute location of any place in chunk
	 * @return
	 */
	private final static String getChunkFileName(Vector location) {
		return "chunk_" + mapFunction(location) + "_i.txt";
	}

	public AreaChunk getAreaChunkForAreaChunk(Vector location) {
		int index = getAreaChunkIndex(location);
		return areachunks[index];
	}

	public static DataChunk getDataChunkForAreaChunk(Map map, Vector location) {
		Vector locale = location.getClone().divideVectorNeg(DataChunkSizeVector);
		return getChunkAt(map, locale);
	}

	/**
	 * Returns DataChunk at given location
	 * @param map Map so get chunk from
	 * @param whichOne Vector of leftMost AreaChunk inside the chunk
	 * @return
	 */
	@Deprecated
	private static DataChunk getChunkAt(Map map, Vector whichOne) {
		DataChunk ac;
		synchronized (loadedChunks) {
			ac = loadedChunks.get(whichOne);
		}
		if (ac != null) {
			return ac;
		} else {
			try {
				String name = getChunkFileName(whichOne);
				File f = new File("MAPS/" + map.getMapFolderName() + "/CHUNKS/");
				if (!f.exists()) {
					f.mkdirs();
				}
				if (!f.isDirectory()) {
					f.mkdirs();
				}
				if (!f.exists() || !f.isDirectory()) {
					System.err.println("Failed to get chunk directory!");
					return null;
				} else {
					File ff = new File(f.getAbsolutePath() + "/" + name);
					if (ff.exists()) { // File exists -> load
						SaverObject obj = Saver.objectFromFile(ff);
						if (obj == null) {
							System.err.println("Failed to read file " + ff.getAbsolutePath());
							return null;
						} else {
							DataChunk c = (DataChunk) new DataChunk(map).restoreFromObject(obj);
							c.map = map;
							c.chunkFile = ff;
							c.allocate();
							return c;
						}
					} else { // File doesn't exist -> new
						DataChunk c = new DataChunk(map, whichOne, ff);
						c.allocate();
						return c;
					}
				}
			} catch (Throwable e) {
				e.printStackTrace();
				return null;
			}
		}
	}

	/**
	 * Allocation preserves the object in memory
	 */
	private void allocate() {
		if (references == 0) { // Fresh load
			synchronized (loadedChunks) {
				loadedChunks.put(this.firstAreaChunk, this);
			}
		}
		references++;
	}

	/**
	 * Transforms any give DataChunk object to key used to map all loaded chunks in static array of all loaded chunks
	 * @param dataChunk
	 * @return
	 */
	private static final Object mapFunction(DataChunk dataChunk) {
		return mapFunction(dataChunk.getLocation());
	}

	/**
	 * Transforms any given vector within chunk to key used to map all loaded chunks in static array of all loaded chunks
	 * @param location
	 * @return
	 */
	private static final Object mapFunction(Vector location) {
		int[] data = location.getClone().getData();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < data.length; i++) {
			sb.append(data[i]);
			sb.append("_");
		}
		if (sb.length() > 0) {
			sb.setLength(sb.length() - 1);
		}
		return sb.toString();
	}

	/**
	 * Deallocation removed the object from memory and stores it to file
	 */
	protected void deallocate() {
		references--;
		checkReferences();
	}

	/**
	 * References are checked and when no references are available, chunk is saved and unlinked
	 */
	private void checkReferences() {
		if (references == 0) { // Save it to file and free all
			save(chunkFile);
			synchronized (loadedChunks) { // Unload the chunk
				loadedChunks.remove(mapFunction(this));
			}
		}
	}

	public AreaChunk getAreaChunk(Vector v) {
		int index = getAreaChunkIndex(v);
		if (areachunks[index] == null) {
			AreaChunk c = new AreaChunk(this, v);
			areachunks[index] = c;
			return c;
		} else {
			return areachunks[index];
		}
	}

	@Deprecated
	public Vector getLocation() {
		return firstAreaChunk;
	}

	public void saveAreaChunk(AreaChunk areaChunk) {
		save(chunkFile);
	}

	@Override
	protected void commitSave(SaverObject s) {
		SaverArray arr = new SaverArray();
		for (AreaChunk chunk : areachunks) {
			SaverObject areadata = new SaverObject();
			chunk.commitSave(areadata);
			arr.addObject(areadata);
		}
		s.addObject("Chunks", arr);
		s.addObject("Location", firstAreaChunk.getSaver());
		SaverArray dar = new SaverArray();
		for (GameObject o : allObjects) {
			dar.addObject(o.getSaver());
		}
		s.addObject("Objects", dar);
	}

	@Override
	public Saveable restoreFromObject(SaverObject obj) {
		java.util.Map<String, Object> aq = obj.getObject(false);
		SaverArray arr = (SaverArray) aq.get("Chunks");
		Object[] data = arr.getObject(false);
		int index = 0;
		Vector location = (Vector) new Vector().restoreFromObject((SaverObject) aq.get("Location"));
		AreaChunk[] areas = new AreaChunk[ChunkSizeTotal];
		DataChunk chnk = new DataChunk(map, location, chunkFile, areas);
		AreaChunk loader = AreaChunk.getLoaderInstance(chnk);
		for (Object o : data) {
			SaverObject areadata = (SaverObject) o;
			AreaChunk chunk = (AreaChunk) loader.restoreFromObject(areadata);
			areas[index] = chunk;
			index++;
		}
		return chnk;
	}

	/**
	 * Contains all GameObjects located on any of the contained Areas
	 */
	private final List<GameObject> allObjects = new ArrayList<>();
	/**
	 * Sublist of allObject, only objects that require ticking
	 */
	private final List<GameObject> tickSensitives = new ArrayList<>();
	private static final int tickCountSave = 1000;
	private static int currentTicks = 0;

	protected void commitDisposal(GameObject o) {
		synchronized (allObjects) {
			allObjects.remove(o);
			if (o.isTickSensitive()) {
				synchronized (tickSensitives) {
					tickSensitives.remove(o);
				}
			}
		}
	}

	protected void commitAppear(GameObject o) {
		synchronized (allObjects) {
			allObjects.add(o);
			if (o.isTickSensitive()) {
				synchronized (tickSensitives) {
					tickSensitives.add(o);
				}
			}
		}
	}

	private static final List<GameObject> toBeDisposed = new ArrayList<>();

	public final void handleTick() {
		synchronized (tickSensitives) {
			for (GameObject o : tickSensitives) {
				o.handleTick();
			}
			currentTicks++;
			if (currentTicks >= tickCountSave) {
				currentTicks = 0;
			}
			synchronized (toBeDisposed) {
				for (GameObject o : toBeDisposed) {
					commitDisposal(o);
				}
				toBeDisposed.clear();
			}
		}
	}

	public void dispose(GameObject gameObject) {
		synchronized (toBeDisposed) {
			map.unregisterPreservableObject(gameObject);
			toBeDisposed.add(gameObject);
		}
	}

	/**
	 * Called on every tick that happens
	 */
	public static final void handleTicks() {
		Object[] chunks;
		synchronized (loadedChunks) {
			chunks = loadedChunks.values().toArray();
		}
		for (Object c : chunks) {
			((DataChunk) c).handleTick();
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("DataChunk [");
		sb.append(areachunks.length);
		sb.append("]: at ");
		sb.append(firstAreaChunk);
		sb.append("\n");
		for (int i = 0; i < areachunks.length; i++) {
			sb.append("  {");
			sb.append(i);
			sb.append("} => ");
			sb.append(areachunks[i]);
			sb.append("\r\n\r\n\r\n");
		}
		if (areachunks.length > 0) {
			sb.setLength(sb.length() - 6);
		}
		sb.append("\r\n\r\n\r\n");
		return sb.toString();
	}

}
