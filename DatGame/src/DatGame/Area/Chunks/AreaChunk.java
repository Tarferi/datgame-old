package DatGame.Area.Chunks;

import DatGame.Area.Area;
import DatGame.Area.Map;
import DatGame.Attributes.Vector;
import DatGame.Saver.Saveable;
import DatGame.Saver.SaveableObject;
import DatGame.Saver.Saver;
import DatGame.Saver.SaverArray;
import DatGame.Saver.SaverObject;

/**
 * Area chunks contain multiple Areas allowing the save and loaded to be done at once
 * This smallest unit to be loaded for ticking GameObject (when Map.loadDimension == 1) 
 * @author Drew
 *
 */
public class AreaChunk extends SaveableObject {

	/**
	 * Number of areas in a row in this chunk
	 * Therefore, if has value 4, then 16 areas are in chunk
	 */
	public static final int ChunkSize = 3;
	/**
	 * Location of first area in chunk
	 */
	private final Vector loc;
	/**
	 * Holder of areas
	 */
	private final Area[] areas;
	private final DataChunk chunk;
	private Map map;
	private final Vector[] vectors;

	private static int ChunkSizeTotal = -1;
	private static int[] mapVector;
	private static final Vector AreaChunkSizeVector = Vector.getUnitVector(ChunkSize);

	public static AreaChunk getLoaderInstance(DataChunk chunk) {
		return new AreaChunk(chunk);
	}

	public Map getMap() {
		return map;
	}

	private int getAreaIndex(Vector v) {
		for (int i = 0; i < vectors.length; i++) {
			if (vectors[i].equals(v)) {
				return i;
			}
		}
		System.err.println("Invalid vector search in AreaChunk");
		return 0;
	}

	public Area getArea(Vector location) {
		int index = getAreaIndex(location.getClone());
		return areas[index];
	}

	@Deprecated
	public Vector getLocation() {
		return loc;
	}

	public void saveArea(Area a) {
		chunk.saveAreaChunk(this);
	}

	static {
		ChunkSizeTotal = 1;
		for (int i = 0, o = Vector.getDimensions(); i < o; i++) {
			ChunkSizeTotal *= ChunkSize;
		}
		mapVector = new int[Vector.getDimensions()];
		for (int i = 0; i < mapVector.length; i++) {
			int power = 1;
			for (int q = 0; q < i; q++) {
				power *= ChunkSize;
			}
			mapVector[i] = power;
		}
	}

	public AreaChunk(DataChunk dataChunk, Vector v) {
		this(dataChunk, v, null);
	}

	/**
	 * Calculates the locations of contained areas
	 * @param left Leftmost (Location with all dimensions set to least possible values)
	 * @return
	 */
	/*
	 * private static Vector[] getAreaVectorArray(Vector left) { Vector[] areas = new Vector[ChunkSizeTotal]; int theSize = ChunkSize; for (int i = 0; i < areas.length; i++) { // For all contained vectors Vector v = left.getClone(); // Clone current vector v.addSubstantionalValue(left, theSize, i); // Shift the current location by i areas[i] = v; } return areas; }
	 */

	private Area[] getNewAreaArray(DataChunk chunk, Vector leftMost) {
		Area[] data = new Area[ChunkSizeTotal];
		Map m = chunk.getMap();
		for (int i = 0; i < data.length; i++) {
			Vector v = vectors[i].getClone();
			Area a = Area.generateArea(v, m, this);
			data[i] = a;
		}

		return data;
	}

	private AreaChunk(DataChunk dataChunk, Vector v, Area[] areas) {
		this.vectors = getAreaChunkVectorArray(v);
		this.chunk = dataChunk;
		this.loc = v;
		if (areas == null) {
			this.areas = getNewAreaArray(dataChunk, v);
		} else {
			this.areas = areas;
		}
	}

	private Vector[] getAreaChunkVectorArray(Vector left) {
		Vector[] areas = new Vector[ChunkSizeTotal];
		int theSize = ChunkSize;
		for (int i = 0; i < areas.length; i++) { // For all contained vectors
			Vector v = left.getClone(); // Clone current vector
			v.addSubstantionalValue(left, theSize, i); // Shift the current location by i
			areas[i] = v;
		}
		return areas;
	}

	/**
	 * Constructs ONLY LOADER object used for spawning other instances
	 * Do NOT use outside loader process and certainly now in any other methods!
	 */
	private AreaChunk(DataChunk chunk) {
		this.map = chunk.getMap();
		this.chunk = chunk;
		loc = null;
		areas = null;
		vectors = null;
	}

	public DataChunk getChunk() {
		return chunk;
	}

	@Override
	protected void commitSave(SaverObject s) {
		SaverArray arr = new SaverArray();
		for (Area area : areas) {
			SaverObject inner = new SaverObject();
			inner.addObject("Area ID", area.getAreaType());
			Saver obj = area.getSaver();
			inner.addObject("Area Data", obj);
			arr.addObject(inner);
		}
		s.addObject("Location", loc);
		s.addObject("Areas", arr);
	}

	private static final Vector vectorLoader = Vector.getZeroVector();

	@Override
	public Saveable restoreFromObject(SaverObject obj) {
		java.util.Map<String, Object> aq = obj.getObject(false);
		SaverArray arr = (SaverArray) aq.get("Areas");
		Object[] objs = arr.getObject(false);
		Area[] areas = new Area[ChunkSizeTotal];
		Vector location = (Vector) vectorLoader.restoreFromObject((SaverObject) aq.get("Location"));
		AreaChunk chnk = new AreaChunk(chunk, location, areas);
		for (int i = 0; i < objs.length; i++) {
			SaverObject inner = (SaverObject) objs[i];
			java.util.Map<String, Object> paq = inner.getObject(false);
			int areaID = (int) paq.get("Area ID");
			SaverObject areaData = (SaverObject) paq.get("Area Data");
			Area area = Area.getFromData(areaID, areaData, chunk.getMap(), chnk);
			areas[i] = area;
		}
		return chnk;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("AreaChunk [");
		sb.append(areas.length);
		sb.append("]: at ");
		sb.append(this.loc);
		sb.append("\n");
		for (int i = 0; i < areas.length; i++) {
			sb.append("  [");
			sb.append(i);
			sb.append("] -> ");
			sb.append(areas[i]);
			sb.append("\r\n");
		}
		if (areas.length > 0) {
			sb.setLength(sb.length() - 2);
		}
		return sb.toString();
	}
/*
	@Deprecated
	public static AreaChunk getChunkAt(Map map, Vector v) {
		DataChunk chunk = DataChunk.getDataChunkForAreaChunk(map, v);
		return chunk.getAreaChunk(v);
	}
*/
	public void deallocate() {
		chunk.deallocate();
	}

	public static AreaChunk getAreaChunkForArea(Map map, Vector location) {
		Vector locale = location.getClone().divideVectorNeg(AreaChunkSizeVector);
		DataChunk chunk = DataChunk.getDataChunkForAreaChunk(map, locale);
		return chunk.getAreaChunkForAreaChunk(locale);
	}
}
