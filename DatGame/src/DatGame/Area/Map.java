package DatGame.Area;

import java.util.HashMap;

import DatGame.AreaLoader;
import DatGame.DatGame;
import DatGame.Area.Chunks.AreaChunk;
import DatGame.Area.Chunks.GameObjectChunk;
import DatGame.Attributes.Vector;
import DatGame.Objects.GameObject;
import DatGame.Objects.Player;
import DatGame.Saver.Saveable;
import DatGame.Saver.SaveableObject;
import DatGame.Saver.SaverObject;

/**
 * Map contains chunks necessary to render map around GameObject that call registerPreservableObject method 
 * @author Drew
 * This is extremely experimental class for N dimensional Map respectful for preserving chunks
 */
public class Map extends SaveableObject {

	/**
	 * Contains all loaded maps indexed by map folder name
	 */
	private static final java.util.Map<String, Map> allMaps = new HashMap<>();

	/**
	 * Contains data of all currently loaded chunks
	 */
	private final java.util.Map<GameObject, GameObjectChunk> loadedChunks = new HashMap<>();

	/**
	 * Contains map folder name, not loaded from file, but file name
	 *
	 */
	private final String MapFolderName;

	public String getMapFolderName() {
		return MapFolderName;
	}

	/**
	 * This is subject to change
	 * @param gameObject Object to keep loaded chunks around for
	 */
	public void registerPreservableObject(GameObject gameObject) {
		synchronized (loadedChunks) {
			if (!loadedChunks.containsKey(gameObject)) {
				GameObjectChunk chunk = new GameObjectChunk(gameObject, this);
				loadedChunks.put(gameObject, chunk);
				chunk.objectMoved();
			} else { // This should never happen when coded with caution
				System.err.println("Registering already registered object, ignoring!");
			}
		}
	}

	public void unregisterPreservableObject(GameObject o) {
		synchronized (loadedChunks) {
			if (loadedChunks.containsKey(o)) {
				GameObjectChunk p = loadedChunks.get(o);
				p.deallocate();
				loadedChunks.remove(o);
			} else { // This should never happen when coded with caution
				System.err.println("Unregistering not registered object, ignoring!");
			}
		}
	}

	public Area getArea(GameObject o, Vector location) {
		if (o == null) {
			System.err.println("Abusing getArea with NULL object!");
			AreaChunk ac = AreaChunk.getAreaChunkForArea(this, location);
			Area a = ac.getArea(location);
			return a;
		} else {
			synchronized (loadedChunks) {
				if (!loadedChunks.containsKey(o)) {
					System.err.println("Requesting area for unregistered object!");
					AreaChunk ac = AreaChunk.getAreaChunkForArea(this, location);
					Area a = ac.getArea(location);
					return a;
				} else {
					GameObjectChunk chunk = loadedChunks.get(o);
					return chunk.getArea(location);
				}
			}
		}
	}

	public static Map loadFromData(AreaLoader areaLoader, DatGame game, SaverObject obj, String MapFolderName) {
		Map m = (Map) new Map(false, MapFolderName).restoreFromObject(obj);
		return m;
	}

	private Map(boolean seriously, String MapFolderName) {
		this.MapFolderName = MapFolderName;
		if (seriously) {
			synchronized (allMaps) {
				allMaps.put(MapFolderName, this);
			}
		}
	}

	@Override
	protected void commitSave(SaverObject s) {
		s.addObject("Map Folder Name", MapFolderName);
	}

	@Override
	public Saveable restoreFromObject(SaverObject obj) {
		return new Map(true, MapFolderName);
	}

	public static Map getMapByFolderName(String mapfolder) {
		System.err.println("Abusing not yet implemented method");
		return null;
	}

	public Area getDefaultArea(GameObject object) {
		return this.getArea(object, Vector.getZeroVector());
	}

	public static void setDefaultPlayerLocation(Player self) {
		self.getLocation().setVector(Vector.getUnitVector(Area.AreaSize / 2));
	}

	public static Map getDefaultMap() {
		return allMaps.entrySet().iterator().next().getValue();
	}

	public GameObjectChunk getGameObjectData(GameObject o) {
		synchronized (loadedChunks) {
			if (!loadedChunks.containsKey(o)) {
				System.err.println("Requesting area for unregistered object!");
				return null;
			} else {
				GameObjectChunk chunk = loadedChunks.get(o);
				loadedChunks.put(o, chunk);
				return chunk;
			}
		}
	}

	public Area getDefaultArea() {
		// TODO Auto-generated method stub
		return null;
	}
}
