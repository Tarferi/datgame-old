package DatGame.Area;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Random;

import DatGame.Area.Chunks.AreaChunk;
import DatGame.Attributes.Location;
import DatGame.Attributes.Size;
import DatGame.Attributes.Vector;
import DatGame.Objects.GameObject;
import DatGame.Objects.Player;
import DatGame.Objects.Projectile;
import DatGame.Objects.WeaponItem;
import DatGame.Saver.Saveable;
import DatGame.Saver.SaverArray;
import DatGame.Saver.SaverObject;

public class GTextArea extends Area {

	private String[] qdata;
	private static final int fontHeight = 12;
	private static int fontWidth = -1;
	private static final Font font = new Font("Monospaced", Font.PLAIN, fontHeight);

	public GTextArea() {
		super(GTextArea.class, Vector.getZeroVector(), null, null);
		qdata = new String[0];
	}

	public GTextArea(Object data, Vector location, Map map, AreaChunk chunk) {
		super(GTextArea.class, location, map, chunk);
		String d = data.toString();
		this.qdata = d.split("\r?\n");
	}

	@Override
	public String getAreaName() {
		return "Text area";
	}

	@Override
	protected void repaint(Graphics g, Vector size) {
		g.setFont(font);
		g.setColor(Color.white);
		if (fontWidth == -1) { // First paint of all, need to determine width of each letter
			fontWidth = g.getFontMetrics().stringWidth("@");
		}
		for (int i = 0; i < qdata.length; i++) {
			g.drawString(qdata[i], 0, (fontHeight * i) + fontHeight);
		}
	}

	private void drawSymbol(Graphics g, Color c, char h, Location l) {
		drawSymbol(g, c, h, l.getValue(0), l.getValue(1));
	}

	private void drawSymbol(Graphics g, Color c, char h, int x, int y) {
		g.setColor(c);
		g.drawString(h + "", x, y);
	}

	@Override
	protected void drawObject(Graphics g, GameObject o) {
		drawSymbol(g, Color.red, '?', o.getLocation());

	}

	@Override
	protected void drawPlayer(Graphics g, Player p) {
		drawSymbol(g, Color.cyan, 'T', p.getLocation());
	}

	@Override
	protected void drawWeapon(Graphics g, WeaponItem p) {
		drawSymbol(g, Color.green, 'W', p.getLocation());
	}

	@Override
	protected void drawProjectile(Graphics g, Projectile p) {
		drawSymbol(g, Color.red, '.', p.getLocation());
	}

	@Override
	protected int getAreaTypeID() {
		return 0;
	}

	private final Random seed = new Random(Area.AreaSize);

	@Override
	protected Area generateNewArea(Vector location, Map map, AreaChunk chunk) {
		int totals = Area.AreaSize / fontHeight;
		StringBuilder sb = new StringBuilder();
		for (int o = 0; o < totals; o++) {
			for (int i = 0, k = totals * 2; i < k; i++) {
				sb.append(seed.nextInt() % 10 == 0 ? '@' : ' ');
			}
			sb.append("\r\n");
		}
		Object data = sb.toString();
		return new GTextArea(data, location, map, chunk);
	}

	@Override
	protected Size getObjectSize(GameObject o) {
		return new Size(o, new Vector().setValue(0, fontWidth).setValue(1, fontHeight));
	}

	@Override
	protected void commitSave(SaverObject s) {
		SaverObject a = new SaverObject();
		super.commitSave(a);
		s.addObject("AreaData", a);
		SaverArray ar = new SaverArray();
		ar.addObject(qdata);
		s.addObject("Data", ar);
	}

	@Override
	public Saveable restoreFromObject(SaverObject obj) {
		java.util.Map<String, Object> aq = obj.getObject(false);
		SaverObject a = (SaverObject) aq.get("AreaData");
		GTextArea q = (GTextArea) super.restoreFromObject(a);
		if (q != null) {
			Object[] objarr = (Object[]) ((SaverArray) aq.get("Data")).getObject(true);
			this.qdata = new String[objarr.length];
			for (int i = 0; i < objarr.length; i++) {
				qdata[i] = (String) objarr[i];
			}
		}
		return q;
	}

}
