package DatGame.Area;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Shape;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import DatGame.Area.Chunks.AreaChunk;
import DatGame.Attributes.Location;
import DatGame.Attributes.Size;
import DatGame.Attributes.Vector;
import DatGame.Objects.GameObject;
import DatGame.Objects.PickableObject;
import DatGame.Objects.Player;
import DatGame.Objects.Projectile;
import DatGame.Objects.WeaponItem;
import DatGame.Saver.Saveable;
import DatGame.Saver.SaveableObject;
import DatGame.Saver.SaverObject;

public abstract class Area extends SaveableObject {

	private static final Font font = new Font("Monospaced", Font.PLAIN, 12);

	private static final Class<?>[] Areas = new Class[] { GTextArea.class, EmptyArea.class };
	public static int TotalAreaTypes = Areas.length;
	private final int areaType;
	private Map map;
	private AreaChunk chunk;

	public static final int AreaSize = 256;
	private Vector location;

	private static final int getAreaID(Class<? extends Area> cls) {
		for (int i = 0; i < Areas.length; i++) {
			if (Areas[i].equals(cls)) {
				return i;
			}
		}
		System.err.println("Failed to determine area type of " + cls);
		return 0;
	}

	public Area(Class<? extends Area> cls, Vector location, Map map, AreaChunk chunk) {
		this.chunk = chunk;
		this.location = location;
		this.areaType = getAreaID(cls);
		this.map = map;
	}

	public Map getMap() {
		return map;
	}

	public Vector getAreaLocation() {
		return location;
	}

	public void paint(Graphics g, int x, int y, int width, int height) {
		g.translate(AreaSize * x, AreaSize * y); // Coordinates 0,0 are left top of current area
		Shape clip = g.getClip(); // Save original clip
		g.setClip(0, 0, AreaSize, AreaSize); // Clip to allow access to current area only
		repaint(g, Vector.getUnitVector(AreaSize)); // Paint the area
		synchronized (objects) {
			for (GameObject o : objects) {
				if (o instanceof Player) {
					drawPlayer(g, (Player) o);
				} else if (o instanceof WeaponItem) {
					drawWeapon(g, (WeaponItem) o);
				} else if (o instanceof Projectile) {
					drawProjectile(g, (Projectile) o);
				} else {
					drawObject(g, o);
				}
			}
		}
		g.setFont(font);
		g.setColor(Color.white);
		g.drawString("x=" + location.getValue(0) + ",y=" + location.getValue(1), 5, 17);
		g.setClip(clip); // Restore clip
		g.translate(-AreaSize * x, -AreaSize * y); // Restore coordinates
	}

	protected abstract void repaint(Graphics g, Vector size);

	protected abstract void drawObject(Graphics g, GameObject o);

	protected abstract void drawPlayer(Graphics g, Player p);

	protected abstract void drawWeapon(Graphics g, WeaponItem p);

	protected abstract void drawProjectile(Graphics g, Projectile p);

	protected abstract Area generateNewArea(Vector location, Map map, AreaChunk chunk);

	protected abstract int getAreaTypeID();

	public abstract String getAreaName();

	public Vector getCenterLocation() {
		return Vector.getUnitVector(AreaSize / 2);
	}

	public static Area getFromData(int id, SaverObject data, Map map, AreaChunk areaChunk) {
		Class<?> qc = Areas[id];
		try {
			Area a = (Area) qc.newInstance();
			Area b = (Area) a.restoreFromObject(data);
			if (b != null) {
				b.map = map;
				b.chunk=areaChunk;
			} else {
				System.err.println("Failed to load area!");
			}
			return b;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Area generateArea(Vector location, Map map2, AreaChunk chunk) {
		int id = (int) (Math.random() * Areas.length * Areas.length);
		id %= Areas.length;
		return generateArea(id, location, map2, chunk);
	}

	@Deprecated
	public static Area generateArea(int id, Vector location, Map map2, AreaChunk chunk) {
		Class<?> qc = Areas[id];
		try {
			Area a = (Area) qc.newInstance();
			Area b = a.generateNewArea(location, map2, chunk);
			return b;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private final List<GameObject> objects = new ArrayList<>();

	public void addGameObject(GameObject gameObject) {
		synchronized (objects) {
			objects.add(gameObject);
		}
	}

	public void removeGameObject(GameObject gameObject) {
		synchronized (objects) {
			objects.remove(gameObject);
		}
	}

	public boolean canMoveTo(Location newloc) {
		return true;
	}

	private static final long mapFunction(Location l) {
		return mapFunction(l.getValue(0), l.getValue(1));
	}

	private static final int pickupRadius = 20;

	private static final OverlapCondition pickableComparator = new OverlapCondition() {

		@Override
		public boolean canOverlap(GameObject obj1, GameObject obj2) {
			return obj2 instanceof PickableObject;
		}

	};

	private static final long mapFunction(int x, int y) {
		return ((x / pickupRadius) << 32) + (y / pickupRadius);
	}

	private final java.util.Map<Long, List<PickableObject>> picks = new HashMap<>();
	private final List<PickableObject> picksList = new ArrayList<>();

	private static OverlapCondition[] emptyConditions = new OverlapCondition[0];

	public void registerPickableObject(PickableObject pickableObject) {
		picksList.add(pickableObject);
		Long key = mapFunction(pickableObject.getLocation());
		if (!picks.containsKey(key)) {
			List<PickableObject> a = new ArrayList<>();
			a.add(pickableObject);
			picks.put(key, a);
		} else {
			picks.get(key).add(pickableObject);
		}
	}

	public void unregisterPickableObject(PickableObject pickableObject) {
		picksList.remove(pickableObject);
		Long key = mapFunction(pickableObject.getLocation());
		if (picks.containsKey(key)) {
			List<PickableObject> a = picks.get(key);
			if (a != null) {
				a.remove(pickableObject);
				if (a.isEmpty()) {
					picks.remove(key);
				}
			}
		}
	}

	protected abstract Size getObjectSize(GameObject o);

	public List<GameObject> getOverlappingObjects(GameObject o) {
		return getOverlappingObjects(o, emptyConditions);
	}

	protected List<GameObject> getOverlappingObjects(GameObject o, OverlapCondition... conditions) {
		List<GameObject> res = new ArrayList<>();
		synchronized (objects) {
			for (GameObject pick : objects) {
				if (pick != o) {
					if (Overlapping(o, pick, conditions)) {
						res.add(pick);
					}
				}
			}
			return res;
		}
	}

	protected interface OverlapCondition {

		public boolean canOverlap(GameObject obj1, GameObject obj2);
	}

	protected boolean Overlapping(GameObject obj, GameObject pick, OverlapCondition... conditions) {
		Size objs = getObjectSize(obj);
		Size pics = getObjectSize(pick);
		int[] poss = objs.getData();
		int[] pwss = obj.getLocation().getData();
		int[] yoss = pics.getData();
		int[] ywss = pick.getLocation().getData();
		for (int i = 0; i < poss.length; i++) {
			int o1w = poss[i];
			int o2w = yoss[i];
			int o1x = pwss[i];
			int o2x = ywss[i];
			if (o1x + o1w < o2x || o2x + o2w < o1x) {
				return false;
			}
		}
		for (OverlapCondition condition : conditions) {
			if (!condition.canOverlap(obj, pick)) {
				return false;
			}
		}
		return true;
	}

	@Override
	protected void commitSave(SaverObject s) {
		s.addObject("Area Type", this.areaType);
		s.addObject("Area type", this.areaType);
		s.addObject("Location", this.location);
	}

	public int getAreaType() {
		return areaType;
	}

	@Override
	public Saveable restoreFromObject(SaverObject obj) {
		java.util.Map<String, Object> aq = obj.getObject(false);
		int type = ((int) aq.get("Area Type"));
		Vector location = (Vector) new Vector().restoreFromObject((SaverObject) aq.get("Location"));
		try {
			Class<?> cls = Areas[type];
			Area b = (Area) cls.newInstance();
			b.location = location;
			return b;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}

	public AreaChunk getChunk() {
		return chunk;
	}

	public void save() {
		this.chunk.saveArea(this);
	}

	public List<GameObject> getPickableObject(Player player) {
		List<GameObject> o = getOverlappingObjects(player, pickableComparator);
		return o;
	}
	
	@Override
	public String toString() {
		return "Area("+this.areaType+") at "+this.location;
	}

}