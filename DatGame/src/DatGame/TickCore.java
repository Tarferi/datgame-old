package DatGame;

import DatGame.UI.DatUIWindow;

public class TickCore {

	private int fps;
	private Thread core;
	private DatGame game;

	private int tps;

	public int getTps() {
		return tps;
		// return fps * (ticks / fps);
	}

	public TickCore(DatGame game, int fps) {
		this.fps = fps;
		this.game = game;
		this.core = new Thread() {
			@Override
			public void run() {
				handleTickCoreThread();
			}
		};
	}

	public void start() {
		core.start();
	}

	public void stop() {
		core.interrupt();
	}

	private int lastMeasurement = 0;
	private int numberOfMeasurements = 1;
	private long start;

	private void handleTickCoreThread() {
		Object syncer = new Object();
		int delay = 1000 / fps;
		DatUIWindow obj = game.getUI();
		synchronized (syncer) {
			while (!core.isInterrupted()) {
				this.start = System.currentTimeMillis();
				obj.redraw();
				int end = (int) (System.currentTimeMillis() % 360000000);
				int start = (int) (this.start % 360000000);
				int calc = end - start;
				int startSec = start / 1000;
				if (lastMeasurement != startSec) { // New range of measurement -> calculate the last
					lastMeasurement = startSec;
					tps = numberOfMeasurements;
					numberOfMeasurements = 0;
				}
				numberOfMeasurements++;
				if (calc < delay) {
					try {
						syncer.wait(delay - calc);
					} catch (InterruptedException e) {
						e.printStackTrace();
						core.interrupt();
					}
				} else {
					System.out.println("Spent too much time on calculations");
				}
			}
		}
	}

	/**
	 * Returns timestamp from last calculation begin (tick) in miliseconds
	 * @return
	 */
	public long getCurrentTick() {
		return start;
	}

}
