package DatGame.Attributes;

import DatGame.Objects.GameObject;

public class Size {

	private GameObject o;
	private Vector v;

	public Size(GameObject o, Vector v) {
		this.v=v;
		this.o=o;
	}
	
	public GameObject getObject() {
		return o;
	}
	
	public int getSizeValue(int dimension) {
		return v.getValue(dimension);
	}
	
	@Override
	public String toString() {
		return "Size of "+o.toString()+": "+v;
	}

	public int[] getData() {
		return v.getData();
	}
}
