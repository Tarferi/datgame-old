package DatGame.Attributes;

import DatGame.Area.Area;
import DatGame.Area.Map;
import DatGame.Objects.GameObject;
import DatGame.Saver.Saveable;
import DatGame.Saver.SaverObject;

public class Location extends Vector {

	private Area area;
	private GameObject object; // !!!Never change!!!
	private final Velocity velocity;

	@Override
	protected void commitSave(SaverObject s) {
		SaverObject a = new SaverObject();
		super.commitSave(a);
		s.addObject("Parent", a);
		s.addObject("Area Location", area.getAreaLocation());
		s.addObject("Velocity", velocity);
		s.addObject("Acceleration", velocity.getAcceleration());
		s.addObject("Map folder name", area.getMap().getMapFolderName());
	}

	@Override
	public Saveable restoreFromObject(SaverObject arr) {
		java.util.Map<String, Object> aq = arr.getObject(false);
		Vector v = (Vector) super.restoreFromObject((SaverObject) aq.get("Parent"));
		Vector arealoc = (Vector) v.restoreFromObject((SaverObject) aq.get("Area Location"));
		Vector velocity = (Vector) v.restoreFromObject((SaverObject) aq.get("Velocity"));
		Vector acc = (Vector) v.restoreFromObject((SaverObject) aq.get("Acceleration"));
		String mapfolder = (String) aq.get("Map folder name");
		Map m = Map.getMapByFolderName(mapfolder);
		Area a = m.getArea(object, arealoc);
		Location loc = new Location(a, v, null);
		loc.velocity.setVector(velocity);
		loc.velocity.getAcceleration().setVector(acc);
		return loc;
	}

	public Location(AbstractLocation loc, GameObject object) {
		super(loc);
		this.area = loc.getArea();
		this.object = object;
		this.velocity = new Velocity(object, Vector.getZeroVector());
	}

	private Location(Area area, Vector location, GameObject object) {
		this.area = area;
		this.object = object;
		this.velocity = new Velocity(object, Vector.getZeroVector());
	}

	public Location getClone(GameObject object) {
		return new Location(area, this, object);
	}

	public Velocity getVelocity() {
		return velocity;
	}

	public GameObject getObject() {
		return object;
	}

	public Area getArea() {
		if (area == null) {
			System.err.println("NULL AREA IN LOCATION!");
			// throw new RuntimeException();
		}
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	/**
	 * Use this strictly for derivating objects
	 * @param p The new object to be assigned
	 */
	public void setObject(GameObject p) {
		if (object != p) { // Altering object -> need to remove old one from area data and add new one
			// area.removeGameObject(object);
			// area.addGameObject(p);
		}
		this.object = p;
		velocity.setObject(p);
	}

	@Override
	public String toString() {
		return "Location: " + super.toString();
	}

}
