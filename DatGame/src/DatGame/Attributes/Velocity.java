package DatGame.Attributes;

import DatGame.Objects.GameObject;

public class Velocity extends Vector {

	private GameObject object;
	private Acceleration acc;

	public Velocity(GameObject object, Vector vector) {
		super(vector);
		this.acc=new Acceleration(object);
		this.object=object;
	}
	
	public Acceleration getAcceleration() {
		return acc;
	}
	
	public GameObject getObject() {
		return object;
	}

	/**
	 * Use only for derivating objects!
	 * @param p
	 */
	public void setObject(GameObject p) {
		this.object=p;
		this.acc.setObject(p);
	}
	
}
