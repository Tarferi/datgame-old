package DatGame.Attributes;

import DatGame.Area.Area;

/**
 * Represent location when no object is known yet
 * @author Drew
 *
 */
public class AbstractLocation extends Vector {

	private Area area;

	public AbstractLocation(Area area, Vector location) {
		super(location);
		this.area = area;
	}

	public Area getArea() {
		return area;
	}

}
