package DatGame.Attributes;

import DatGame.Objects.GameObject;

public class Acceleration extends Vector {

	private GameObject o;

	public Acceleration(GameObject o) {
		this.o=o;
	}
	
	public GameObject getObject() {
		return o;
	}

	/**
	 * Use strictly for derivating objects!
	 * @param p
	 */
	public void setObject(GameObject p) {
		this.o=p;
	}
	
}
