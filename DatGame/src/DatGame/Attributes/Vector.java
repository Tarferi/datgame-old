package DatGame.Attributes;

import java.util.Map;

import DatGame.Saver.Saveable;
import DatGame.Saver.SaveableObject;
import DatGame.Saver.SaverArray;
import DatGame.Saver.SaverObject;

public class Vector extends SaveableObject {

	/**
	 * Dimensional data
	 */
	private int[] dims;

	/**
	 * Number of dimensions
	 */
	private static int Dimension;
	/**
	 * Contains all 1
	 */
	private static int[] UnitDimension;
	/**
	 * Contain all 0
	 */
	private static int[] ZeroDimension;

	public static final void setDimension(int dimensions) {
		Dimension = dimensions;
		UnitDimension = new int[Dimension];
		ZeroDimension = new int[Dimension];
		for (int i = 0; i < Dimension; i++) {
			UnitDimension[i] = 1;
			ZeroDimension[i] = 0;
		}
	}

	public static int getDimensions() {
		return Dimension;
	}

	public int[] getData() {
		return dims;
	}

	public Vector() {
		dims = new int[Dimension];
	}

	private static final int[] getClonedDimensions(int[] v) {
		int[] dims = new int[Dimension];
		System.arraycopy(v, 0, dims, 0, Dimension);
		return dims;
	}

	public Vector(Vector vector) {
		dims = getClonedDimensions(vector.dims);
	}

	/**
	 * Private constructor, when Dimension is known and respected
	 * @param data
	 */
	private Vector(int[] data) {
		dims = data;
	}

	public Vector getClone() {
		return new Vector(this);
	}

	public Vector addVector(Vector v) {
		for (int i = 0; i < Dimension; i++) {
			dims[i] += v.dims[i];
		}
		return this;
	}

	public Vector setVector(Vector v) {
		dims = getClonedDimensions(v.dims);
		return this;
	}

	public Vector subtractVector(Vector v) {
		for (int i = 0; i < Dimension; i++) {
			dims[i] -= v.dims[i];
		}
		return this;
	}

	public Vector multiplyVector(Vector v) {
		for (int i = 0; i < Dimension; i++) {
			dims[i] *= v.dims[i];
		}
		return this;
	}

	public Vector moduloVector(Vector v) {
		for (int i = 0; i < Dimension; i++) {
			dims[i] %= v.dims[i];
		}
		return this;
	}

	@Deprecated
	public Vector divideVector(Vector v) {
		for (int i = 0; i < Dimension; i++) {
			dims[i] /= v.dims[i];
		}
		return this;
	}

	public static Vector getZeroVector() {
		return new Vector(getClonedDimensions(ZeroDimension));
	}

	public static Vector getUnitVector() {
		return new Vector(getClonedDimensions(UnitDimension));
	}

	public static Vector getUnitVector(int val) {
		int[] vals = new int[Dimension];
		for (int i = 0; i < Dimension; i++) {
			vals[i] = val;
		}
		return new Vector(vals);
	}

	public int getValue(int dimension) {
		return dims[dimension];
	}

	public Vector setValue(int dimension, int value) {
		dims[dimension] = value;
		return this;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < Dimension; i++) {
			sb.append(dims[i]);
			sb.append(", ");
		}
		if (sb.length() > 0) {
			sb.setLength(sb.length() - 2);
		}
		return "Vector: " + sb.toString();
	}

	@Override
	protected void commitSave(SaverObject s) {
		SaverArray data = new SaverArray();
		data.addObject(dims);
		s.addObject("Vector Data", data);
	}

	@Override
	public Saveable restoreFromObject(SaverObject arr) {
		Map<String, Object> aq = arr.getObject(true);
		Object[] a = (Object[]) aq.get("Vector Data");
		if (a.length != dims.length) {
			System.err.println("Not enough dimensions for " + arr);
		}
		int[] m = new int[a.length];
		for (int i = 0; i < a.length; i++) {
			m[i] = (int) a[i];
		}
		return new Vector(m);
	}

	@Deprecated
	public Vector removeModuloValue(Vector v) {
		for (int i = 0; i < Dimension; i++) {
			dims[i] -= (dims[i] % v.dims[i]);
		}
		return this;
	}

	/**
	 * Adjust current vector in Cuboid of Length size by X value from Base
	 * Used for iterating through common cuboid
	 * @param Base First location in cuboid
	 * @param Length Size of cuboid
	 * @param X Shift ammount
	 */
	public Vector addSubstantionalValue(Vector Base, int Length, int X) {
		for (int i = 0; i < dims.length; i++) {
			int base = Base.dims[i];
			int limit = base + Length;
			int value = dims[i];
			if (value + X >= limit) { // Increment higher dimension, adjust the current one
				value -= base; // Align to zero coordinates
				value += X; // Adjust as designed
				value %= Length; // Remove higher dimension affection
				value += base; // Align to original coordinates
				dims[i] = value;
			} else { // Current dimension is to be affected only
				value += X;
				dims[i] = value;
				break;
			}
			X /= Length;
		}
		return this;
	}

	/**
	 * Divides by vector and keeps rounding down, not to 0 as regular division
	 * @param v
	 * @return
	 */
	public Vector divideVectorNeg(Vector v) {
		for (int i = 0; i < dims.length; i++) {
			if (dims[i] < 0) {
				if (dims[i] % v.dims[i] != 0) { // This is the breaking point of rounding up
					dims[i] -= v.dims[i];
				}
				dims[i] /= v.dims[i];
			} else {
				dims[i] /= v.dims[i];
			}
		}
		return this;
	}

	@Override
	public boolean equals(Object v) {
		if (v instanceof Vector) {
			for (int i = 0; i < dims.length; i++) {
				if (dims[i] != ((Vector) v).dims[i]) {
					return false;
				}
			}
			return true;
		} else {
			return super.equals(v);
		}
	}
}
