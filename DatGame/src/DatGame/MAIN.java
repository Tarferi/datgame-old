package DatGame;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class MAIN {

	public static void main(String[] argv) {
		setLNF();
		new DatGame();
	}

	public static void setLNF() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
	}

}
