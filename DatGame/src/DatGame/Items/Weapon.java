package DatGame.Items;

import java.awt.Color;
import java.awt.Graphics;

import DatGame.DatGame;
import DatGame.Attributes.Location;
import DatGame.Attributes.Vector;
import DatGame.Inventory.Inventory;
import DatGame.Inventory.InventoryObject;
import DatGame.Objects.GameObject;
import DatGame.Objects.Projectile;

public abstract class Weapon extends InventoryObject {

	private Vector firePower;
	private Vector fireAcceleration;
	private String name;

	public Weapon(DatGame game, Inventory inv, String name, Vector firePower, Vector fireAcceleration) {
		super(inv);
		this.fireAcceleration = fireAcceleration;
		this.firePower = firePower;
		this.name = name;
	}

	public String getWeaponName() {
		return name;
	}

	public abstract Projectile getProjectile(DatGame game, GameObject shooter);

	public void fire(Vector look) {
		GameObject holder = super.getInventory().getHolder();
		Projectile p = getProjectile(holder.getGame(), holder); // Spawn the projectile
		Location old = holder.getLocation();
		Location loc = new Location(old.getArea(), old, p);
		loc.addVector(look); // Add look vector so the projectile will be spawned in front of holder
		loc.getVelocity().setVector(look); // Default velocity
		loc.getVelocity().multiplyVector(firePower); // Set default velocity
		loc.getVelocity().getAcceleration().setVector(look); // Default acceleration
		loc.getVelocity().getAcceleration().multiplyVector(fireAcceleration); // Set default acceleration
		p.setLocation(loc); // Teleport to right position
	}

	@Override
	public String toString() {
		return "Unknown weapon";
	}

	@Override
	public String getTitle() {
		return toString();
	}

	@Override
	public void redraw(Graphics g, int width, int height) {
		g.setColor(Color.green);
		g.fillRect(0, 0, width, height);
	}

}
