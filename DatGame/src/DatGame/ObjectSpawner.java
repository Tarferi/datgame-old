package DatGame;

import DatGame.Area.Area;
import DatGame.Attributes.Location;
import DatGame.Attributes.Vector;
import DatGame.Objects.WeaponItem;
import DatGame.Objects.WeaponObjects.PistolObject;

public class ObjectSpawner {

	private int tickPeriod = 100;
	private int currentTick = 0;
	private int maxRadius = 500;
	private int minRadius = 100;
	private DatGame game;

	public void handleTick() {
		currentTick++;
		if (currentTick >= tickPeriod) {
			currentTick = 0;
			handleSpawn();
		}
	}

	public ObjectSpawner(DatGame game) {
		this.game = game;
	}

	private void handleSpawn() {
		int range = maxRadius - minRadius;
		int vX = (int) ((((Math.random() * 2) - 1) * range) + minRadius);
		int vY = (int) ((((Math.random() * 2) - 1) * range) + minRadius);
		Location loc = game.getSelfLocation().getClone(game.getSelfLocation().getObject());
		// Move to proper area within the map
		loc.setArea(loc.getArea().getMap().getArea(game.getSelf(), Vector.getZeroVector().setValue(0, (vX / Area.AreaSize) + game.getSelfLocation().getArea().getAreaLocation().getValue(0)).setValue(1, (vY / Area.AreaSize) + game.getSelfLocation().getArea().getAreaLocation().getValue(1))));
		// Move to proper coordinates withing the area
		loc.setValue(0, vX % Area.AreaSize);
		loc.setValue(1, vY % Area.AreaSize);
		WeaponItem w = new PistolObject(game, -1, null);
		loc.setObject(w);
		w.setLocation(loc);
		// System.out.println("Spawning pistol at " + vX + ":" + vY);
	}

}
