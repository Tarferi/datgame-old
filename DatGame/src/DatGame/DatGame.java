package DatGame;

import DatGame.Area.Map;
import DatGame.Attributes.AbstractLocation;
import DatGame.Attributes.Location;
import DatGame.Attributes.Vector;
import DatGame.Inventory.InventoryObject;
import DatGame.Objects.GameObject;
import DatGame.Objects.Player;
import DatGame.UI.DatUIWindow;

public class DatGame {

	private AreaLoader areaLoader;
	private DatUIWindow ui;
	private TickCore tl;
	private Player self;
	private ObjectSpawner os;

	public DatGame() {
		Vector.setDimension(2); // 2D game
		areaLoader = new AreaLoader(this);
		areaLoader.load();
		os = new ObjectSpawner(this);
		tl = new TickCore(this, 200);
		if (self == null) {
			spawnSelf();
		}
		ui = new DatUIWindow(this);
		start();
	}

	public ObjectSpawner getObjectSpawner() {
		return os;
	}

	private void spawnSelf() {
		AbstractLocation loc = new AbstractLocation(Map.getDefaultMap().getDefaultArea(), Vector.getZeroVector());
		self = new Player(this, "", loc, -1);
		loc.setObject(self);
		self.getLocation().getArea().getMap().registerPreservableObject(self);
		Map.setDefaultPlayerLocation(self);
	}

	private void start() {
		tl.start();
	}

	public DatUIWindow getUI() {
		return ui;
	}

	public TickCore getTickCore() {
		return tl;
	}

	public AreaLoader getAreaLoader() {
		return areaLoader;
	}

	public Location getSelfLocation() {
		return self.getLocation();
	}

	public void selfMove(Vector vector) {
		vector.multiplyVector(Vector.getUnitVector(4)); // Movement acceleration
		self.getLocation().getVelocity().getAcceleration().setVector(vector);
	}

	public void setSelf(GameObject o) {
		this.self = (Player) o;
	}

	public Player getSelf() {
		return self;
	}

	public void selfFire(InventoryObject o, Vector lookVector) {
		self.useItem(o, lookVector);
	}
}
