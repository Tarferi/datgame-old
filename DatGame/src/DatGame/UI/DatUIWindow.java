package DatGame.UI;

import javax.swing.JFrame;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;

import DatGame.DatGame;
import DatGame.Area.Chunks.DataChunk;
import DatGame.Attributes.Vector;
import DatGame.Inventory.InventoryObject;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class DatUIWindow extends JFrame {

	public void redraw() {
		DataChunk.handleTicks();
		game.getObjectSpawner().handleTick();
		mapPanel.redraw();
		inventory.redraw();
	}

	public void refreshInventory() {
		inventory.refresh();
	}

	private static final long serialVersionUID = 6599144309534471621L;
	private DatUIMap mapPanel;
	private DatGame game;
	private final KeyListener keyadapter = new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			DatUIWindow.this.keyPressed(e.getKeyChar(), e.getKeyCode());
		}

		@Override
		public void keyTyped(KeyEvent e) {
			DatUIWindow.this.keyPressed(e.getKeyChar(), e.getKeyCode());
		}
	};
	private DatUIInventory inventory;

	public DatUIWindow(DatGame game) {
		setTitle("DatGame");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.game = game;
		construct();
		setVisible(true);
	}

	public void construct() {
		setSize(640, 480);
		addKeyListener(keyadapter);
		getContentPane().setLayout(new BorderLayout(0, 0));
		getContentPane().addKeyListener(keyadapter);
		JPanel BottomPanel = new JPanel();
		getContentPane().add(BottomPanel, BorderLayout.SOUTH);
		BottomPanel.setLayout(new MigLayout("", "[grow]", "[75][]"));

		inventory = new DatUIInventory(game);
		BottomPanel.add(inventory, "cell 0 0,alignx left,growy");

		JLabel lblTest = new JLabel("Test");
		BottomPanel.add(lblTest, "cell 0 1");

		mapPanel = new DatUIMap(game, this);
		mapPanel.addKeyListener(keyadapter);
		getContentPane().add(mapPanel, BorderLayout.CENTER);
	}

	private void keyPressed(char keyChar, int keyCode) {
		switch (keyChar) {
			case 'w':
				game.selfMove(Vector.getZeroVector().setValue(1, -1));
			break;
			case 'a':
				game.selfMove(Vector.getZeroVector().setValue(0, -1));
			break;
			case 's':
				game.selfMove(Vector.getZeroVector().setValue(1, 1));
			break;
			case 'd':
				game.selfMove(Vector.getZeroVector().setValue(0, 1));
			break;
		}

		switch (keyCode) {
			case KeyEvent.VK_UP:
				game.selfMove(Vector.getZeroVector().setValue(1, -1));
			break;
			case KeyEvent.VK_LEFT:
				game.selfMove(Vector.getZeroVector().setValue(0, -1));
			break;
			case KeyEvent.VK_DOWN:
				game.selfMove(Vector.getZeroVector().setValue(1, 1));
			break;
			case KeyEvent.VK_RIGHT:
				game.selfMove(Vector.getZeroVector().setValue(0, 1));
			break;
			case KeyEvent.VK_SPACE:
				InventoryObject item = inventory.getActiveItem();
				if (item != null) {
					game.selfFire(item, mapPanel.getLookVector());
				}
			break;
		}
	}
}
