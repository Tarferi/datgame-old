package DatGame.UI;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;

import DatGame.DatGame;
import DatGame.Inventory.InventoryItemStack;
import DatGame.Inventory.InventoryObject;

public class DatUIInventory extends JPanel {

	private static final Color clr_sel = Color.orange;
	private static final Color clr_def = Color.WHITE;
	private static final Color clr_act = Color.yellow;

	public void redraw() {
		content.revalidate();
		content.repaint();
		for (DatUIInventoryItem i : items) {
			i.redraw();
		}
	}

	private DatUIInventoryItem selected = null;

	private final MouseListener mouseListener = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			DatUIInventoryItem src = (DatUIInventoryItem) e.getSource();
			if (src != selected) {
				if (selected != null) {
					selected.setBackground(clr_def);
				}
				src.setBackground(clr_sel);
				selected = src;
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			DatUIInventoryItem src = (DatUIInventoryItem) e.getSource();
			if (src != selected) {
				src.setBackground(clr_act);
			}
		}

		@Override
		public void mouseExited(MouseEvent e) {
			DatUIInventoryItem src = (DatUIInventoryItem) e.getSource();
			if (src != selected) {
				src.setBackground(clr_def);
			}
		}
	};

	private final List<DatUIInventoryItem> items = new ArrayList<>();

	public void refresh() {
		List<InventoryItemStack> inv = game.getSelf().getInventory().getAllObjects();
		int i = 0;
		items.clear();
		content.removeAll();
		java.util.Map<String, Integer> itms = new HashMap<>();
		for (InventoryItemStack oq : inv) {
			InventoryObject o = oq.getItem().getInventoryObject();
			String t = o.getTitle();
			if (itms.containsKey(t)) {
				itms.put(t, itms.get(t).intValue() + 1);
			} else {
				itms.put(t, 1);
			}
			DatUIInventoryItem q = new DatUIInventoryItem(oq);
			q.setSize(50, 50);
			content.add(q);
			q.setLocation(50 * i, 0);
			q.setBackground(clr_def);
			q.setBorder(new LineBorder(Color.black, 1));
			q.addMouseListener(mouseListener);
			i++;
			items.add(q);
		}
	}

	private JPanel content;
	private DatGame game;

	public DatUIInventory(DatGame game) {
		this.game = game;
		setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		content = new JPanel();
		scrollPane.setViewportView(content);
	}

	private static final long serialVersionUID = 270434036484769833L;

	public InventoryObject getActiveItem() {
		if (selected != null) {
			return selected.getItem();
		} else {
			return null;
		}
	}

}
