package DatGame.UI;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;

import javax.swing.JPanel;

import DatGame.DatGame;
import DatGame.GameGraphic;
import DatGame.Attributes.Vector;

public class DatUIMap extends JPanel {
	private static final int rtCircleSize = 15;
	private final Vector look = new Vector();

	private static final long serialVersionUID = 6307466677255938837L;
	private DatGame game;
	private GameGraphic graphics;

	public DatUIMap(DatGame game, DatUIWindow ui) {
		this.graphics = new GameGraphic(ui);
		this.game = game;
	}

	public void redraw() {
		this.revalidate();
		this.repaint();
	}

	private void drawTPS(Graphics2D g) {
		int tpsborder = 5;
		int tpsoffset = 10;
		String tps = "Current TPS: " + game.getTickCore().getTps();
		int tpsw = g.getFontMetrics().stringWidth(tps);
		int tpsh = g.getFont().getSize();
		g.translate(tpsoffset, tpsoffset);
		g.setColor(Color.cyan);
		Stroke original = g.getStroke();
		g.setStroke(new BasicStroke(4));
		g.setColor(Color.cyan);
		g.drawRect(-2, -2, tpsborder + tpsborder + tpsw + 4, tpsborder + tpsborder + tpsh + 4);
		g.setStroke(original);
		g.setColor(Color.red);
		g.fillRect(0, 0, tpsborder + tpsborder + tpsw, tpsborder + tpsborder + tpsh);
		g.setColor(Color.white);
		g.drawString(tps, tpsborder, tpsborder + tpsh);
		g.translate(-tpsoffset, -tpsoffset);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if (g instanceof Graphics2D) {
			Graphics2D gg = (Graphics2D) g;
			AffineTransform origin = gg.getTransform();
			graphics.paint(game, gg, getWidth(), getHeight()); // Paint the map
			gg.setTransform(origin);
			drawTPS(gg);
			int w2 = getWidth() / 2;
			int h2 = getHeight() / 2;
			gg.translate(w2, h2);
			gg.setColor(Color.white);
			gg.drawOval(-rtCircleSize, -rtCircleSize, rtCircleSize + rtCircleSize, rtCircleSize + rtCircleSize);
			Point p = this.getMousePosition();
			if (p != null) {
				double mx = p.getX() - w2;
				double my = p.getY() - h2;
				double length = Math.sqrt((mx * mx) + (my * my)); // Pythagoras
				double minilength = rtCircleSize + 10; // Circle radius
				double ratio = minilength / length;
				double nx = (ratio * mx);
				double ny = (ratio * my);
				gg.drawLine(0, 0, (int) nx, (int) ny);
				look.setValue(0, (int) (nx / rtCircleSize)); // TODO: Results in 0 for many values!
				look.setValue(1, (int) (ny / rtCircleSize)); // TODO: Fix
			}
		}
	}

	public Vector getLookVector() {
		return look;
	}
}
