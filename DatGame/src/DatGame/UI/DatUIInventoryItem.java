package DatGame.UI;

import java.awt.Graphics;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;

import DatGame.Inventory.InventoryItemStack;
import DatGame.Inventory.InventoryObject;

public class DatUIInventoryItem extends JPanel {
	private JPanel customGraphics;
	private JLabel label;
	private InventoryItemStack obj;

	public void redraw() {
		customGraphics.revalidate();
		customGraphics.repaint();
		label.setText(obj.getItem().getInventoryObject().getTitle() + " (" + obj.getCount() + ")");
	}

	public DatUIInventoryItem(final InventoryItemStack o) {
		this.obj = o;
		setLayout(new MigLayout("", "[106.00,grow][][grow]", "[][][grow]"));

		label = new JLabel("<WeaponType>");
		add(label, "cell 0 0 3 1,alignx center");

		label.setText(obj.getItem().getInventoryObject().getTitle());

		customGraphics = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paint(Graphics g) {
				obj.getItem().getInventoryObject().redraw(g, getWidth(), getHeight());
			}
		};
		add(customGraphics, "cell 0 2 3 1,grow");
	}

	private static final long serialVersionUID = -1384663067133833343L;

	public InventoryObject getItem() {
		return obj.getItem().getInventoryObject();
	}

}
