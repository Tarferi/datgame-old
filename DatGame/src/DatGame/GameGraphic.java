package DatGame;

import java.awt.Color;
import java.awt.Graphics;

import DatGame.Area.Area;
import DatGame.Area.Map;
import DatGame.Attributes.Vector;
import DatGame.UI.DatUIWindow;

public class GameGraphic {

	private DatUIWindow window;

	public GameGraphic(DatUIWindow window) {
		this.window = window;
	}

	public DatUIWindow getWindow() {
		return window;
	}

	int renderDistance = 7;

	// TODO: Rewrite to N dimensional space
	public void paint(DatGame game, Graphics g, int width, int height) {
		// Draw all areas
		int tX = 0, tY = 0;
		int[] dims = game.getSelfLocation().getData();
		tX -= dims[0];
		tY -= dims[1];
		g.setColor(Color.black);
		g.fillRect(0, 0, width, height);
		// g.translate(tX-(Area.AreaSize/2), tY-Area.AreaSize/2);
		int hd = -renderDistance / 2;
		Area a = game.getSelfLocation().getArea();
		g.translate(tX + (width / 2) + (hd * Area.AreaSize), tY + (height / 2) + (hd * Area.AreaSize));
		Map m = a.getMap();
		int x = a.getAreaLocation().getValue(0);
		int y = a.getAreaLocation().getValue(1);
		Area[] areas = new Area[renderDistance * renderDistance];
		int center = renderDistance / 2;
		for (int i = 0; i < renderDistance; i++) {
			for (int o = 0; o < renderDistance; o++) {
				//TODO: Rewrite to N dimensional space
				areas[i * renderDistance + o] = m.getArea(game.getSelf(), Vector.getZeroVector().setValue(0, x + o - center).setValue(1, y + i - center));
			}
		}
		/*
		 * areas[0] = m.getArea(x - 1, y - 1); areas[1] = m.getArea(x, y - 1); areas[2] = m.getArea(x + 1, y - 1); areas[3] = m.getArea(x - 1, y); areas[4] = m.getArea(x, y); areas[5] = m.getArea(x + 1, y); areas[6] = m.getArea(x - 1, y + 1); areas[7] = m.getArea(x, y + 1); areas[8] = m.getArea(x + 1, y + 1);
		 */
		for (int i = 0; i < areas.length; i++) {
			int xx = i % renderDistance;
			int yy = i / renderDistance;
			areas[i].paint(g, xx, yy, width, height);
		}
	}
}
