package DatGame.Objects;

import DatGame.DatGame;

/**
 * This object is special kind of object that is only placeholder for future GameObject subclass
 * Never use this for any other purpose. It cannot be saved nor loaded!
 * @author Drew
 *
 */
public class VoidObject extends GameObject {

	public VoidObject(DatGame game) {
		super(game, -1, null);
	}

	@Override
	protected String getEntityName() {
		return null;
	}

}
