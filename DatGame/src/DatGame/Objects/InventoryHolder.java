package DatGame.Objects;

import DatGame.DatGame;
import DatGame.Attributes.Location;
import DatGame.Inventory.Inventory;
import DatGame.Saver.SaverObject;

public abstract class InventoryHolder extends LivingObject {

	private final Inventory inventory;

	public InventoryHolder(DatGame game, SaverObject obj) {
		super(game, obj);
		this.inventory = new Inventory(this);
	}

	public InventoryHolder(DatGame game, int entityID, Location loc) {
		super(game, entityID, loc);
		this.inventory = new Inventory(this);
	}

	/**
	 * Clear inventory when entity dies
	 */
	@Override
	public void die() {
		inventory.clean();
		super.die();
	}

	public Inventory getInventory() {
		return inventory;
	}

}
