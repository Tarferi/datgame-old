package DatGame.Objects;

import java.util.Map;

import DatGame.DatGame;
import DatGame.Attributes.Location;
import DatGame.Saver.SaverObject;

public abstract class LivingObject extends MovableObject {

	private int maxHealth;
	private int minHealth;
	private int healthPerTick;
	private int health;

	public LivingObject(DatGame game, SaverObject obj) {
		super(game, (SaverObject) obj.getObject(false).get("Parent"));
		Map<String, Object> aq = obj.getObject(false);
		maxHealth=(int) aq.get("maxHealth");
		minHealth=(int) aq.get("minHealth");
		healthPerTick=(int) aq.get("healthPerTick");
		health=(int) aq.get("health");
	}
	
	@Override
	protected void commitSave(SaverObject s) {
		SaverObject parent=new SaverObject();
		super.commitSave(parent);
		s.addObject("Parent", parent);
		s.addObject("maxHealth", maxHealth);
		s.addObject("minHealth", minHealth);
		s.addObject("healthPerTick", healthPerTick);
		s.addObject("health", health);
	}


	/*
	 * 
	 * @Override protected Object getSaveData() { return maxHealth + "\r\n" + minHealth + "\r\n" + healthPerTick + "\r\n" + health + "\r\n" + super.getSaveData(); }
	 * 
	 * @Override protected void restoreData(Object dataa) { String[] b = dataa.toString().split("\r\n", 5); try { int maxh = Integer.parseInt(b[0]); int minh = Integer.parseInt(b[1]); int hptk = Integer.parseInt(b[2]); int curh = Integer.parseInt(b[3]); maxHealth = maxh; minHealth = minh; healthPerTick = hptk; health = curh; } catch (Exception e) { e.printStackTrace(); } super.restoreData(b[4]); }
	 */

	@Override
	public void handleTick(long tick) {
		super.handleTick(tick);
		if (health < maxHealth) {
			if (health + healthPerTick > maxHealth) {
				health = maxHealth;
			} else {
				health = health + healthPerTick;
			}
		}
		if (health < minHealth) {
			die();
		}
	}

	public void die() {
		dispose();
	}

	public int getMaxHealth() {
		return maxHealth;
	}

	public int getMinHealth() {
		return minHealth;
	}

	public int getHealthPerTick() {
		return healthPerTick;
	}

	public int getHealth() {
		return health;
	}

	public void addHealth(int h) {
		setHealth(health + h);
	}

	public void hurt(int howMuch) {
		addHealth(-howMuch);
		if (health == minHealth) {
			die();
		}
	}

	public void setHealth(int health) {
		if (health > maxHealth) {
			health = maxHealth;
		} else if (health < minHealth) {
			health = minHealth;
		}
		this.health = health;
	}

	public void setHealthPerTick(int healthPerTick) {
		this.healthPerTick = healthPerTick;
	}

	public void setMinHealth(int minHealth) {
		this.minHealth = minHealth;
	}

	public void setMaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
	}

	public LivingObject(DatGame game, int entityID, Location loc) {
		super(game, entityID, loc);
	}

	@Override
	public boolean hitByProjectile(Projectile projectile) {
		hurt(projectile.getDamage());
		return true;
	}
}
