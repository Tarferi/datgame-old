package DatGame.Objects.WeaponObjects;

import DatGame.DatGame;
import DatGame.Attributes.Location;
import DatGame.Inventory.InventoryObject;
import DatGame.Objects.InventoryHolder;
import DatGame.Objects.WeaponItem;
import DatGame.Objects.Weapons.Pistol;
import DatGame.Saver.SaverObject;

public class PistolObject extends WeaponItem {

	public PistolObject(DatGame game, int entityID, Location loc) {
		super(game, entityID, loc, "Pistol");
	}

	public PistolObject(DatGame game, SaverObject data) {
		super(game, data);
	}

	@Override
	protected String getEntityName() {
		return "Weapon_Pistol";
	}

	@Override
	protected InventoryObject getPickedObject(InventoryHolder picker) {
		return new Pistol(super.getGame(), picker.getInventory());
	}

}
