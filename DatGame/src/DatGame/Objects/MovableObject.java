package DatGame.Objects;

import DatGame.DatGame;
import DatGame.Area.Area;
import DatGame.Area.Chunks.GameObjectChunk;
import DatGame.Attributes.Location;
import DatGame.Attributes.Vector;
import DatGame.Saver.Saveable;
import DatGame.Saver.SaverObject;

public abstract class MovableObject extends GameObject {

	private GameObjectChunk rc;

	/**
	 * Last received tick
	 */
	private long lastTick = System.currentTimeMillis();
	/**
	 * Number of miliseconds between movements
	 */
	private static final int movementMilis = 25;
	/**
	 * Actual number of miliseconds waiting for movement
	 */
	private int movementWaiting = 0;

	public MovableObject(DatGame game, int entityID, Location l) {
		super(game, entityID, l);
	}

	@Override
	protected void objectInit() {
		rc = getLocation().getArea().getMap().getGameObjectData(this);
	}

	public MovableObject(DatGame game, SaverObject saverObject) {
		super(game, saverObject);
	}

	@Override
	protected void commitSave(SaverObject o) {
		super.commitSave(o);
	}

	@Override
	public Saveable restoreFromObject(SaverObject obj) {
		return null;
	}

	protected Vector getMaximalAcceleration() {
		return Vector.getUnitVector(10);
	}

	protected Vector getMaximalVelocity() {
		return Vector.getUnitVector(10);
	}

	public void moveToNewArea() {
		Location location = getLocation();
		Area current = location.getArea(); // Current area
		int[] poss = location.getData(); // Current location vector
		Vector mov = Vector.getZeroVector(); // Area Movement vector
		for (int i = 0; i < poss.length; i++) { // For all dimensions
			int pos = poss[i]; // Current dimension
			int move;
			if (pos < 0) { // Moving left
				move = (-1 + ((int) pos / Area.AreaSize)); // How many areas need to be skipped
				pos %= Area.AreaSize; // Position in current area
				pos = Area.AreaSize + pos;
			} else { // Moving right
				move = (int) pos / Area.AreaSize; // How many areas need to be skipped
				pos %= Area.AreaSize; // Position in current area
			}
			mov.setValue(i, move); // Update movement vector data
			location.setValue(i, pos); // Update location in current dimension
		}
		Area nw = current.getMap().getArea(this, current.getAreaLocation().getClone().addVector(mov)); // Get new area
		if (current != nw) { // Moving from area to another area for sure
			current.removeGameObject(this);
			nw.addGameObject(this);
		}
		location.setArea(nw); // Set new area
		setLocation(location);
	}

	/**
	 * Every tick calls this function on all objects
	 * Movement is done here
	 */
	public final void handleTick() {
		Location location = getLocation();
		int[] poss = location.getData();
		int[] maxacc = getMaximalAcceleration().getData();
		int[] maxvel = getMaximalVelocity().getData();
		Location newloc = location.getClone(this);
		long currentTick = getGame().getTickCore().getCurrentTick(); // Current tick in miliseconds
		int newWait = (int) (currentTick - lastTick); // Number of miliseconds between current and last tick
		movementWaiting += newWait; // Total time waiting for movements
		if (movementWaiting > movementMilis) { // Can move
			int moveRatio = movementWaiting / movementMilis; // Number of times to move
			for (int currentMovement = 0; currentMovement < moveRatio; currentMovement++) {
				handleTick(currentTick);
				movementWaiting %= movementMilis;
				for (int i = 0; i < poss.length; i++) {
					int pos = poss[i];
					if (pos < 0 || pos > Area.AreaSize) {
						moveToNewArea();
						currentMovement--;
						continue;
					} else {
						if (newloc.getArea().canMoveTo(newloc)) {
							int velocity = location.getVelocity().getValue(i);
							int acceleration = location.getVelocity().getAcceleration().getValue(i);
							int maxacceleration = maxacc[i];
							int maxvelocity = maxvel[i];
							if (velocity != 0 || acceleration != 0) { // At least one derivation of location must be non zero
								location.setValue(i, location.getValue(i) + velocity);
								if (velocity > 0 && acceleration < 0 && velocity + acceleration < 0) {
									velocity = acceleration = 0;
								} else if (velocity < 0 && acceleration > 0 && velocity + acceleration > 0) {
									velocity = acceleration = 0;
								} else {
									velocity += acceleration;
									// acceleration /= 10;
									// acceleration = acceleration < 0.1 ? 0 : acceleration / 10;
									// velocity = velocity < 0.1 ? 0 : velocity / 10;
									// TODO: New physics
									// acceleration += acceleration > 0 ? -1 : acceleration < 0 ? 1 : 0;
									// velocity += velocity > 0 ? -1E-14 : velocity < 0 ? 1E-14 : 0;
									// velocity*=1E-50;
								}
								velocity = velocity > maxvelocity ? maxvelocity : -velocity > maxvelocity ? -maxvelocity : velocity;
								acceleration = acceleration > maxacceleration ? maxacceleration : -acceleration > maxacceleration ? -maxacceleration : acceleration;
								location.getVelocity().setValue(i, velocity);
								location.getVelocity().getAcceleration().setValue(i, acceleration);
								objectMoved();
							}
						} else {
							location.getVelocity().setValue(i, 0);
							location.getVelocity().getAcceleration().setValue(i, 0);
						}
					}
				}
			}
		}
		lastTick = currentTick;
	}

	@Override
	public boolean isTickSensitive() {
		return true;
	}

}
