package DatGame.Objects;

import java.util.List;

import DatGame.DatGame;
import DatGame.Attributes.Location;
import DatGame.Attributes.Vector;
import DatGame.Inventory.InventoryObject;
import DatGame.Items.Weapon;
import DatGame.Saver.SaverObject;

public class Player extends InventoryHolder {

	@Override
	protected void objectMoved() {
		List<GameObject> picks = super.getLocation().getArea().getPickableObject(this);
		if (picks != null) {
			for (GameObject o : picks) {
				if (o instanceof PickableObject) {
					((PickableObject) o).pick(this);
				}
			}
		}
	}

	private String name;

	public Player(DatGame game, String name, Location loc, int entityID) {
		super(game, entityID, loc);
		this.name = name;
		super.setHealth(20);
		super.setMinHealth(0);
		super.setMaxHealth(20);
	}

	public String getPlayName() {
		return name;
	}

	@Override
	public void die() {
		super.die();
	}

	@Override
	protected String getEntityName() {
		return "Player";
	}

	@Override
	public String toString() {
		return "Player(" + name + ")";
	}

	/**
	 * Called when use item has been pressed and inventory has active item
	 * @param lookVector
	 */
	public void useItem(InventoryObject o, Vector lookVector) {
		if (o instanceof Weapon) {
			Weapon w = (Weapon) o;
			w.fire(lookVector);
		}
	}

	@Override
	protected void commitSave(SaverObject s) {
		SaverObject o = new SaverObject();
		super.commitSave(o);
		s.addObject("Player Name", name);
		s.addObject("Parent", o);
	}

	public Player(DatGame game, SaverObject obj) {
		super(game, (SaverObject) obj.getObject(false).get("Parent"));
		java.util.Map<String, Object> aq = obj.getObject(false);
		this.name = (String) aq.get("Player Name");
	}
}
