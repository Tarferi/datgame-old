package DatGame.Objects;

import DatGame.DatGame;
import DatGame.Attributes.AbstractLocation;
import DatGame.Attributes.Location;
import DatGame.Attributes.Vector;
import DatGame.Objects.WeaponObjects.PistolObject;
import DatGame.Saver.Saveable;
import DatGame.Saver.SaveableObject;
import DatGame.Saver.SaverObject;

public abstract class GameObject extends SaveableObject {

	private Location location;
	private final DatGame game;
	private int entityID;
	private static int maxEntityID = 0;

	protected void objectMoved() {
	}

	protected void objectSpawned() {
	}

	private void updateEntityIDS(int entityID2) {
		if (entityID2 > maxEntityID) {
			maxEntityID = entityID2 + 1;
		}
	}

	private int getNextEntityID() {
		maxEntityID++;
		return maxEntityID;
	}

	// Called when object has been created and located and all data are valid
	protected void objectInit() {

	}

	public static GameObject restoreEntity(DatGame game, String entityName, SaverObject data) {
		GameObject o = null;
		switch (entityName) {
			case "Player":
				o = (GameObject) new Player(game, data);
			break;
			case "Weapon_Pistol":
				o = (GameObject) new PistolObject(game, data);
			break;

		}
		return o;
	}

	protected abstract String getEntityName();

	/**
	 * Dispose the object
	 */
	public void dispose() {
		location.getArea().getChunk().getChunk().dispose(this);
	}

	/**
	 * Use only when location is not known when creating object!
	 * @param loc
	 */
	public void setLocation(Location loc) {
		if (loc != location) {
			if (location != null) { // Teleport
				location.getArea().removeGameObject(this);
				location.setArea(loc.getArea());
				location.setVector(loc);
				location.getArea().addGameObject(this);
			} else { // Spawn
				location = new Location(loc.getArea(), loc, this);
				location.getVelocity().setVector(loc.getVelocity());
				location.getVelocity().getAcceleration().setVector(loc.getVelocity().getAcceleration());
				location.getArea().addGameObject(this);
				objectSpawned();
			}
		}
	}

	public GameObject(DatGame game, SaverObject obj) {
		java.util.Map<String, Object> aq = obj.getObject(false);
		int entityid = (int) aq.get("Entity ID");
		location = new Location(null, Vector.getZeroVector(), this);
		Location loc = (Location) location.restoreFromObject((SaverObject) aq.get("Location"));
		AbstractLocation qloc = new AbstractLocation(loc.getArea(), loc);
		boolean self = (boolean) aq.get("Self");
		this.game = game;
		Constructor(entityid, qloc);
		if (self) {
			game.setSelf(this);
		}
	}

	public GameObject(DatGame game, int entityID, AbstractLocation l) {
		this.game = game;
		Constructor(entityID, l);
	}

	private final void Constructor(int entityID, AbstractLocation l) {
		this.entityID = entityID;
		Location loc = new Location(l, this);
		this.location = loc;
		if (l != null) {
			if (l.getArea() != null) {
				location.getArea().addGameObject(this);
			}
		} else {
			System.err.println("Spawning GameObject with NULL location");
		}
		if (entityID == -1) { // Object being created
			this.entityID = getNextEntityID();
			updateEntityIDS(this.entityID);
			if (l != null) {
				objectSpawned();
			}
		} else { // Object being loaded
			updateEntityIDS(this.entityID);
		}
		objectInit();
	}

	public DatGame getGame() {
		return game;
	}

	public Location getLocation() {
		return location;
	}

	/**
	 * Called when hit by projectile
	 * @param projectile
	 * @return True if projectile is to be stopped by current object, false to keep it going
	 */
	public boolean hitByProjectile(Projectile projectile) {
		// So what? I'm a common object...
		return false;
	}

	@Override
	protected void commitSave(SaverObject s) {
		s.addObject("Entity ID", entityID);
		s.addObject("Location", location);
		if (this instanceof Player) {
			s.addObject("Self", entityID == ((GameObject) game.getSelf()).entityID);
		} else {
			s.addObject("Self", false);
		}
	}

	@Override
	public Saveable restoreFromObject(SaverObject obj) {
		System.err.println("Abusing restore method of GameObject");
		return null;
	}

	/**
	 * Returns True if object requires tick handling 
	 * @return
	 */
	public boolean isTickSensitive() {
		return false;
	}

	/**
	 * Every game tick calls this method
	 */
	public void handleTick() {

	}

	protected void handleTick(long tick) {

	}

}
