package DatGame.Objects;

import java.util.Map;

import DatGame.DatGame;
import DatGame.Attributes.Location;
import DatGame.Saver.SaverObject;

public abstract class WeaponItem extends PickableObject {

	private String name;

	@Override
	public void handleTick(long tick) {
		super.handleTick(tick); // Movement and everything
	}

	public String getWeaponName() {
		return name;
	}

	public WeaponItem(DatGame game, int entityID, Location loc, String name) {
		super(game, entityID, loc);
		this.name = name;
	}

	@Override
	protected void commitSave(SaverObject s) {
		SaverObject o = new SaverObject();
		super.commitSave(o);
		s.addObject("Weapon name", name);
		s.addObject("Parent", o);
	}

	public WeaponItem(DatGame game, SaverObject data) {
		super(game, (SaverObject) data.getObject(false).get("Parent"));
		Map<String, Object> aq = data.getObject(false);
		name = (String) aq.get("Weapon name");
	}
}
