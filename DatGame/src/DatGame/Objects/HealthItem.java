package DatGame.Objects;

import DatGame.DatGame;
import DatGame.Attributes.Location;
import DatGame.Inventory.InventoryObject;

public class HealthItem extends PickableObject {
	private static final int healthToAdd=20;

	public HealthItem(DatGame game, int entityID, Location l) {
		super(game, entityID, l);
	}

	@Override
	protected InventoryObject getPickedObject(InventoryHolder picker) {
		picker.addHealth(healthToAdd);
		return null;
	}

	@Override
	protected String getEntityName() {
		return "Health";
	}

}
