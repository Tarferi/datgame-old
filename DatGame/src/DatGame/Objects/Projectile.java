package DatGame.Objects;

import java.util.List;

import DatGame.DatGame;
import DatGame.Area.Area;
import DatGame.Attributes.Vector;
import DatGame.Items.Weapon;

public abstract class Projectile extends MovableObject {

	private final Weapon source;
	private int tickCount;
	private final int damage;
	private final GameObject shooter;

	@Override
	protected boolean canBeSaved() {
		return false;
	}
	
	@Override
	protected Vector getMaximalAcceleration() {
		return Vector.getUnitVector(60);
	}

	@Override
	protected Vector getMaximalVelocity() {
		return Vector.getUnitVector(60);
	}
	
	@Override
	public void handleTick(long tick) {
		super.handleTick(tick); // Movement etc
		if (tickCount == 0) { // Dispose the projectile?
			dispose();
		} else {
			tickCount--;
		}
		Area a = getLocation().getArea();
		List<GameObject> o = a.getOverlappingObjects(this);
		if (!o.isEmpty()) {
			for (GameObject obj : o) {
				if (obj != shooter) {
					if (obj.hitByProjectile(this)) {
						return;
					}
				}
			}
		}
	}

	public int getDamage() {
		return damage;
	}

	public Projectile(Weapon source, DatGame game, GameObject shooter, int entityID, int MaxTickCount, int damage) {
		super(game, entityID, null);
		this.damage = damage;
		this.source = source;
		this.tickCount = MaxTickCount;
		this.shooter = shooter;
	}

	public Weapon getSourceWeapon() {
		return source;
	}

}
