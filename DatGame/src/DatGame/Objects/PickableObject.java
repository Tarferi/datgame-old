package DatGame.Objects;

import DatGame.DatGame;
import DatGame.Attributes.Location;
import DatGame.Inventory.InventoryObject;
import DatGame.Saver.SaverObject;

public abstract class PickableObject extends GameObject {

	@Override
	public void objectSpawned() {
		super.getLocation().getArea().registerPickableObject(this);
	}

	/**
	 * Called when object is being picked up
	 * @param picker Picker of the item
	 * @return Inventory object to be added to inventory of picked or null if nothing is to be added
	 */
	protected abstract InventoryObject getPickedObject(InventoryHolder picker);

	public void pick(InventoryHolder o) {
		super.getLocation().getArea().unregisterPickableObject(this);
		InventoryObject q = getPickedObject(o);
		if (q != null) {
			o.getInventory().addItem(q);
		}
		dispose();
	}

	public PickableObject(DatGame game, int entityID, Location l) {
		super(game, entityID, l);
	}

	public PickableObject(DatGame game, SaverObject saverObject) {
		super(game,saverObject);
	}

}
