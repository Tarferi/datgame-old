package DatGame.Objects.Weapons;

import DatGame.DatGame;
import DatGame.Attributes.Vector;
import DatGame.Inventory.Inventory;
import DatGame.Items.Weapon;
import DatGame.Objects.GameObject;
import DatGame.Objects.Projectile;
import DatGame.Objects.Projectiles.PistolShot;

public class Pistol extends Weapon {

	@Override
	public Projectile getProjectile(DatGame game, GameObject shooter) {
		return new PistolShot(this, game, shooter, -1, 50);
	}

	public Pistol(DatGame game, Inventory inv) {
		super(game, inv, "Pistol", Vector.getUnitVector(4), Vector.getUnitVector(5));
	}

	@Override
	public String toString() {
		return "Pistol";
	}
}
