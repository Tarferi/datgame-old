package DatGame.Objects.Projectiles;

import DatGame.DatGame;
import DatGame.Items.Weapon;
import DatGame.Objects.GameObject;
import DatGame.Objects.Projectile;

public class PistolShot extends Projectile {

	public PistolShot(Weapon source, DatGame game, GameObject shooter, int entityID, int MaxTickCount) {
		super(source, game, shooter, entityID, MaxTickCount, 1);
	}

	@Override
	protected String getEntityName() {
		return "Projectile_PistolShot";
	}

}
