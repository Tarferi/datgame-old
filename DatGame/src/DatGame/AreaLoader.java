package DatGame;

import java.io.File;

import DatGame.Area.Area;
import DatGame.Area.Map;
import DatGame.Area.Chunks.AreaChunk;
import DatGame.Attributes.Vector;
import DatGame.Saver.Saver;
import DatGame.Saver.SaverObject;

public class AreaLoader {
	private final String mapFolder = "MAPS";
	private final DatGame game;

	public AreaLoader(DatGame datGame) {
		this.game = datGame;
	}

	public void load() {
		File f = new File(mapFolder);
		if (f.exists()) {
			File[] files = f.listFiles();
			for (File d : files) {
				loadMap(d);
			}
		} else {
			System.err.println("Map folder does not exist");
		}
	}

	public void loadMap(File f) {
		File data = new File(f.getAbsolutePath() + "/" + "map.txt");
		SaverObject obj = Saver.objectFromFile(data);
		if (obj != null) {
			Map.loadFromData(this, game, obj, f.getName());
		}
	}

	@Deprecated
	public Area getAreaAt(Map m, Vector v) {
		AreaChunk ac = AreaChunk.getAreaChunkForArea(m, v);
		if (ac == null) {
			System.err.println("Failed to get area chunk at " + v);
			return null;
		}
		Area a = ac.getArea(v);
		if (a == null) {
			System.err.println("Failed to get area at " + v);
			return null;
		}
		return a;
	}
}
