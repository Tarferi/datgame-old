package DatGame.Inventory;

import java.awt.Graphics;

/**
 * This object represents GameObject that can be stored in Inventory
 * This is the other possible variation of GameObject and conversion is one way only
 * @author Drew
 *
 */
public abstract class InventoryObject {

	private Inventory inv;

	private InventoryItem item;

	/**
	 * Do not use outside inventory class!
	 * @return
	 */
	protected InventoryItem getItem() {
		return item;
	}

	/**
	 * Do not use outside inventory class!
	 * @param item
	 */
	protected void setItem(InventoryItem item) {
		this.item = item;
	}

	public InventoryObject(Inventory inv) {
		this.inv = inv;
	}

	public Inventory getInventory() {
		return inv;
	}

	@Override
	public String toString() {
		return "Unknown Object";
	}


	public abstract String getTitle();

	public abstract void redraw(Graphics g, int width, int height);
}
