package DatGame.Inventory;

/**
 * This object represents item in inventory.
 * This is transparent to inventory holders and only contains inventory informations
 * @author Drew
 *
 */
public class InventoryItem {

	private Inventory inv;
	private InventoryObject obj;
	private InventoryItemStack stack;

	public InventoryItem(Inventory inv, InventoryObject o) {
		o.setItem(this);
		this.inv = inv;
		this.obj = o;
	}

	protected InventoryItemStack getInventoryItemStack() {
		return stack;
	}

	protected void setInventoryItemStack(InventoryItemStack s) {
		this.stack = s;
	}

	public InventoryObject getInventoryObject() {
		return obj;
	}

	public Inventory getInventory() {
		return inv;
	}

	@Override
	public String toString() {
		return "InventoryItem(" + obj + ")";
	}

}
