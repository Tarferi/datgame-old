package DatGame.Inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import DatGame.DatGame;
import DatGame.Objects.InventoryHolder;

public class Inventory {

	private InventoryHolder obj;
	private final java.util.Map<String, InventoryItemStack> items = new HashMap<>();
	private DatGame game;

	public Inventory(InventoryHolder obj) {
		this.obj = obj;
		this.game = obj.getGame();
	}

	public InventoryHolder getHolder() {
		return obj;
	}

	public void addItem(InventoryObject o) {
		String title = o.getTitle();
		synchronized (items) {
			if (items.containsKey(title)) {
				items.get(title).addItem(new InventoryItem(this, o));
			} else {
				items.put(title, new InventoryItemStack(this, new InventoryItem(this, o)));
			}
			game.getUI().refreshInventory();
			System.out.println("Received new inventory itemt: " + this);
		}
	}

	public void removeItem(InventoryObject o) {
		String title = o.getTitle();
		synchronized (items) {
			if (items.containsKey(title)) {
				InventoryItemStack itms = items.get(title);
				itms.removeItem(o.getItem());
				if (itms.isEmpty()) {
					items.remove(title);
				}
			}
			game.getUI().refreshInventory();
		}
	}

	public void clean() {
		items.clear();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Inventory of " + obj + ", Content: ");
		synchronized (items) {
			for (Entry<String, InventoryItemStack> obj : items.entrySet()) {
				sb.append(obj.getKey());
				sb.append("(");
				sb.append(obj.getValue().getCount());
				sb.append("), ");
			}
			if (!items.isEmpty()) {
				sb.setLength(sb.length() - 2);
			}
		}
		return sb.toString();
	}

	public List<InventoryItemStack> getAllObjects() {
		List<InventoryItemStack> s = new ArrayList<>();
		for (InventoryItemStack obj : items.values()) {
			s.add(obj);
		}
		return s;
	}
}
