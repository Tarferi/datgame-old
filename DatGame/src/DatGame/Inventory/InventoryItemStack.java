package DatGame.Inventory;

import java.util.ArrayList;
import java.util.List;

public class InventoryItemStack {

	private InventoryItem item;
	private final List<InventoryItem> allItems = new ArrayList<>();
	private int count;
	private Inventory inventory;

	public InventoryItemStack(Inventory inventory, InventoryItem item) {
		item.setInventoryItemStack(this);
		this.inventory = inventory;
		this.item = item;
		allItems.add(item);
		this.count = 1;
	}

	public void addItem(InventoryItem item) {
		allItems.add(item);
		item.setInventoryItemStack(this);
		count++;
	}

	public boolean isEmpty() {
		return count == 0;
	}

	public void removeItem(InventoryItem item) {
		allItems.remove(item);
		count--;
		if (item == this.item) { // Instance item must remain oen of the contained ones
			if (allItems.size() > 0) {
				this.item = allItems.get(0);
			} else {
				this.item = null;
			}
		}
	}

	public Inventory getInventory() {
		return inventory;
	}

	public int getCount() {
		return count;
	}

	public InventoryItem getItem() {
		return item;
	}

	public List<InventoryItem> getAllItems() {
		return allItems;
	}
}
